# $Id: fit_data_k4pln.py,v 1.3 2008/06/17 20:02:34 kris Exp kris $

import pickle
import mrc_models
import kh_models
import math
from numpy import sum,array,log10
from scipy.optimize import fmin_l_bfgs_b, anneal

data = pickle.load(open('mrc_kh_pickle','r'))
fplnresults = pickle.load(open('fpln_fit_results_pickle','r'))
kfplnresults = pickle.load(open('kfpln_fit_results_pickle','r'))


# wrapper for calling 4-parameter ln mrc and kh routines
def kfpln_ls(pin, *args):
    hk = array(args[0]['h']) 
    k = array(args[0]['k']) 
    ht = array(args[1]['h'])
    t = array(args[1]['t'])

    pout = zeros((8,))
    pout[0] = pin[0]*pin[1]  # thrmult*ths = thr
    pout[1] = pin[1]
    pout[2] = 10.0**(pin[2]+pin[3]) # r0_mult*rmax = r0
    pout[3] = 10.0**pin[3] # rmax
    pout[4] = pin[4]  # sigma
    pout[5] = pin[5]  # mu
    pout[6] = 10.0**pin[6] # ksat
    pout[7] = pin[7] # L

    rest = sum((mrc_models.fourpln(pout[0:6],ht) - t)**2)/len(ht)
    resk = sum((log10(kh_models.fourplnk(pout,hk)) - log10(k))**2)/len(hk)

    return args[2]*rest + resk

# results
duallnresults = {}
tolerance = 75.0

# loop over soils 
for id in fplnresults:
    #  thrmult  ths  r0mult  rmax  sig   mu    ksat  L  
    minp =[0.0, 0.1, -40.0, -8.0,  0.0, -20.0, -7.0, -10.0]
    maxp =[0.8, 0.6,  -1.0,  20.0, 7.0,   1.0,  7.0,  10.0]

    # use results of independent optimzation as initial guess
    p = [fplnresults[id]['x1'][0],fplnresults[id]['x1'][1],
         kfplnresults[id]['x'][0],kfplnresults[id]['x'][1],
         fplnresults[id]['x1'][2],fplnresults[id]['x1'][3],
         kfplnresults[id]['x'][2],kfplnresults[id]['x'][3]]

    # mainly optimize mrc
    weight = 10.0
    xa1,da = anneal(kfpln_ls,p,lower=minp,upper=maxp,
                   args=[data[id]['kh'],data[id]['mrc'],weight])

    args = [data[id]['kh'],data[id]['mrc'],weight]
    fa1 = kfpln_ls(xa1,*args)

##    # somewhere between
##    weight = 2.0
##    xa1,da = anneal(kfpln_ls,xa1,lower=minp,upper=maxp,
##                   args=[data[id]['kh'],data[id]['mrc'],weight])
##
##    args = [data[id]['kh'],data[id]['mrc'],weight]
##    fa1 = kfpln_ls(xa1,*args)
##
####    x1,f1,d1 = fmin_l_bfgs_b(kfpln_ls,xa1,bounds=zip(minp,maxp), 
####                          approx_grad=1,args=[data[id]['kh'],
####                                              data[id]['mrc'],weight])

    # lean more towards K(h)
    weight = 1.0
    xa2,da = anneal(kfpln_ls,xa1,lower=minp,upper=maxp,
                   args=[data[id]['kh'],data[id]['mrc'],weight])

    args = [data[id]['kh'],data[id]['mrc'],weight]
    fa2 = kfpln_ls(xa2,*args)

    x,f,d = fmin_l_bfgs_b(kfpln_ls,xa2,bounds=zip(minp,maxp), 
                          approx_grad=1,args=[data[id]['kh'],
                                              data[id]['mrc'],weight])

    print id,fa1,fa2,f

    duallnresults[id] = {'x':x,'obj':f,'info':d,'xa':xa2,'obja':fa2}


pickle.dump(duallnresults, open('dual_fit_results_pickle','w'))
