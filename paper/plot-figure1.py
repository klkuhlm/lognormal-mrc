import numpy as np
import matplotlib.pyplot as plt
import compare_mrc_functions as ln

h = np.linspace(0,20,300)
sigma = [0.5,1.0,2.0]
colors = ['red','green','blue']

mu = -4.75
rmax = 0.1
r0 = 0.005

hc = ln.kappa/rmax
hmax = ln.kappa/r0

print 'hc',hc,'hmax',hmax

theta = np.zeros_like(h)
Kr = np.zeros_like(h)

fig = plt.figure(1,figsize=(4,4))
ax = fig.add_subplot(111)
alfs = 18

# single figure

for j in range(len(sigma)):
    for i in range(h.shape[0]):
        theta[i] = ln.MRC2(h[i],sigma[j],mu)
        Kr[i] = ln.Kr2(theta[i],h[i],sigma[j],mu)

    ax.plot(h,Kr,color=colors[j],linewidth=2)

    for i in range(h.shape[0]):
        theta[i] = ln.MRC3(h[i],sigma[j],mu,rmax)
        Kr[i] = ln.Kr3(theta[i],h[i],sigma[j],mu,rmax)

    line = ax.plot(h,Kr,color=colors[j],linewidth=2)[0]
    line.set_dashes((6,2))

    ##for i in range(h.shape[0]):
    ##    theta[i] = ln.MRC4(h[i],sigma[j],mu,rmax,r0)
    ##    Kr[i] = ln.Kr4(theta[i],h[i],sigma[j],mu,rmax,r0)
    ##
    ##line = ax.plot(h,Kr,color=colors[j],linewidth=2)[0]
    ##line.set_dashes((3,3))

ax.set_xlabel('$h$ [cm]',fontsize=alfs)
ax.set_xlim([0,20])
ax.set_ylim([0,1])
ax.tick_params(axis='both', which='major', labelsize=13)

ax.set_ylabel('$K_r(h)$',fontsize=alfs)
        
ax.annotate('$\\sigma^2_Z=0.25$',xy=(8,0.9),fontsize=14)
ax.annotate('$\\sigma^2_Z=1$',xy=(5.5,0.6),fontsize=14)
ax.annotate('$\\sigma^2_Z=4$',xy=(3,0.15),fontsize=14)

plt.tight_layout()
plt.savefig('figure1.pdf')
plt.savefig('figure1.png')
