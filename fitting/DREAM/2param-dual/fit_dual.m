function [S] = fit_dual(par,extra)

  p = par(1:4);
  p(5) = 10.0^par(5); % K0

  modtheta = mrc_2param_model(p,extra.ht);
  modk      = kh_2param_model(p,extra.hk);

  S = [log10(modk),modtheta];