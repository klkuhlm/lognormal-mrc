function [modtheta] = fit_soils(par,extra)

  p = par;
  p(3) = 10.0^par(3); % rmax
  
  modtheta = mrc_3param_model(p,extra.ht);
  