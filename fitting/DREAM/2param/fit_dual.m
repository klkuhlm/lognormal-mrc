function [S] = fit_dual(par,extra)

  p = par(1:6);
  p(3) = 10.0^par(3);
  p(4) = 10.0^par(4);
  p(7) = 1.0; % K0
  p(8) = 0.5; % L

  modtheta = mrc_model(p,extra.ht);
  modk      = kh_model(p,extra.hk);

  S = [log10(modk),modtheta];