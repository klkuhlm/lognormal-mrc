# $Id: fit_data_vg.py,v 1.3 2008/06/17 19:06:08 kris Exp kris $

import pickle
import mrc_models 
from numpy import sum, array
from scipy import optimize

data = pickle.load(open('mrc_kh_pickle','r'))

# wrapper for calling mrc routine
def vg_ls(p, *args):
    h = array(args[0]['h'])  # vector of observed water content
    t = array(args[0]['t'])  # corresponding vector of pressure head
    
    return sum((mrc_models.vgt(p,h) - t)**2)

# results
results = {}

fres = open('vgt.out','w')

# loop over soils 
for id in data:
    
    # initial guess from avarage parameter of Schaap 2006
    
    #       thr    ths    alpha   n
    p0 =   [0.055, 0.442, 0.0219, 1.64]
    minp = [0.0,   0.0,   1.0E-4, 1.0 ]
    maxp = [0.3,   0.6,   None,   None]
    
    x,f,d = optimize.fmin_l_bfgs_b(vg_ls, \
             p0,approx_grad=1,bounds=zip(minp,maxp),args=[data[id]['mrc']])
    
    results[id] = {'x':x,'obj':f,'info':d}

    fres.write(id + ' ' + str(x) + ' ' + str(f) + '\n')
    

fres.close()
pickle.dump(results, open('vgt_fit_results_pickle','w'))
