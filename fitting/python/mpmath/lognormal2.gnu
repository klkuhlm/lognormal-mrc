set xrange [1e-1:1e4];
set yrange [1e-4:1e-1];
unset format
set format y "10^{%T}"
set format x "10^{%T}"
unset label;
unset logscale;
set logscale x; set xtics 1e-3,1e1,1e5
set logscale y; set ytics 1e-12,1e1,1e6

set xlabel "h (cm)";
set ylabel "f_H(h)"; # 2,0;
x_m = 1e-1;
z_m = -6.80;
sig = 1.0;
k = 0.149;
f1(x) = exp(-0.5*((log(x)-log(k)+z_m)/sig)**2)/(x*sig*sqrt(2.0*pi));
f2(x) = exp(-0.5*((log(x-k/x_m)-log(k)+z_m)/sig)**2)/((x-k/x_m)*sig*sqrt(2.0*pi));
f3(x) = exp(-0.5*((log(1/(1/x-x_0/k)-k/x_m)-log(k)+z_m)/sig)**2)/((1/(1/x-x_0/k)-k/x_m)*sqrt(2.0*pi)*sig);

unset key

set key
set size 1,1.2
set output 'densityz.eps'
set term postscript eps enhanced 'Helvetica' 28 mono dashed
plot f1(x) t 'classical' w l lw 4 lt 1, \
     x_0=1e-4,f3(x) t 'proposed' w l lw 2 lt 3, \
     f2(x) t 'Kosugi (1994)' w l lw 2 lt 2