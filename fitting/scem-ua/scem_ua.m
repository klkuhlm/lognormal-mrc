function [ParSet,GR,Sequences] = scem_ua(SCEMPar,ParRange,Measurement,ModelName,Extra,option);
% --------------- The Shuffled Complex Evolution Metropolis algorithm -------------------
% Input arguments:
%
% SCEMPar			Structure with algorithmic parameters
% ParRange			Structure with minimum and maximum of individual parameters
% Measurement		Structure with measurement data and error deviation
% ModelName			Name of the model to be run
% Extra			Structure with additional arguments that need to be passed to run model
% option 			Type of option used to compute likelihood of model

% Output arguments
%
% ParSet			2D array with accepted parameter values [1:n] and associated likelihood [n+1]
% GR				2D array with Gelman Rubin convergence criteria
% Sequences			Parameters generated in the individual sequences, last column is likelihood
%
%               Written by Jasper A. Vrugt, Tucson, AZ, July 2001 -- various modifications thereafter
%  
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = 

% Initialize two important variables
Iter = SCEMPar.s; counter  = 1;									

% Calculate the number of points in each complex, m and the number of offspring generated per iteration, L
SCEMPar.m = floor(SCEMPar.s./SCEMPar.q); SCEMPar.L = max([1,floor(SCEMPar.m./5)]);

% Sample s points in the parameter space using latin hypercube
x = lhsu(ParRange.minn,ParRange.maxn,SCEMPar.s);

% Calculate the parameters in the exponential power density function of Box and Tiao
[SCEMPar.Wb,SCEMPar.Cb] = calccbwb(SCEMPar.Gamma);

% Calculate posterior densities of each parameter set depending on the chosen option
[pset] = computedensity(x,SCEMPar,Measurement,ModelName,Extra,option);

% Save the output in the array ParSet
ParSet = [x,pset(1:end,1)];

% Sort the points in order of decreasing posterior probability
D = -sortrows(-pset,1);

% Initialize starting points of sequences 
Sequences = initsequences(D,SCEMPar,x);

% Compute convergence
[Rstat] = gelman(Sequences,SCEMPar); GR = [Iter Rstat];

% Iterate until a predefined number of function evaluations has been readched ...
while (Iter < SCEMPar.ndraw),

    % Partition D into c complexes A
    C = partcomplexes(D,SCEMPar,x);

    % Evolve each sequence with the Sequence Evolution Metropolis (SEM) algorithm
    [C,Sequences,ParSet] = sem(C,Sequences,ParSet,ParRange,SCEMPar,Measurement,ModelName,Extra,option,Iter);
    
    % Reshuffle the points
    [D,x] = reshuffle(C,SCEMPar);
    
    % Compute convergence
    [Rstat] = gelman(Sequences,SCEMPar);
    
    % Update the Iteration
    Iter = Iter + SCEMPar.L*SCEMPar.q
    
    % Now save the convergence statistic
    GR = [GR; Iter Rstat];

end;