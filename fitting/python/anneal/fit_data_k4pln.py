# $Id: fit_data_k4pln.py,v 1.3 2008/06/17 20:02:34 kris Exp kris $

import pickle
import kh_models
import math
from numpy import array, log10, zeros
from scipy.optimize import fmin_l_bfgs_b, anneal

data = pickle.load(open('mrc_kh_pickle','r'))
fplnresults = pickle.load(open('fpln_fit_results_pickle','r'))


# wrapper for calling 4-parameter ln routine
def kfpln_ls(pin, *args):
    h = array(args[0]['h']) 
    k = array(args[0]['k']) 

    pout = zeros((8,))
    pout[0] = args[1][0]*args[1][1] # thrmult*ths = thr
    pout[1] = args[1][1] # ths
    pout[2] = 10.0**(pin[1]+pin[0]) # r0_mult
    pout[3] = 10.0**pin[1] # rmax
    pout[4] = args[1][2] # sig_Z
    pout[5] = args[1][3] # mu_Z
    pout[6] = 10.0**pin[2] # ksat
    pout[7] = pin[3] # L

    return sum((log10(kh_models.fourplnk(pout,h)) - log10(k))**2)

# results
kfplnresults = {}
tolerance = 75.0

# loop over soils 
for id in fplnresults:
 
    # maximum r0 and minimum rmax come from search done in
    # mrc-fitting routine.  These were found so that the mrc fit 
    # does not degrade at the expense of the fit chosen here

    #          r0_mult, rmax, K0, L
    minp = [-40.1, math.log10(fplnresults[id]['r'][1]), -7.0, -10.0]
    maxp = [math.log10(fplnresults[id]['r'][0]/fplnresults[id]['r'][1]), 
            20.1, 7.0,10.0]
    p =    [math.log10(fplnresults[id]['r'][0]/fplnresults[id]['r'][1]), 
            math.log10(fplnresults[id]['r'][1]), math.log10(data[id]['ks2']), 0.5]

    # use defaults for most cases
    xa,da = anneal(kfpln_ls,p,lower=minp,upper=maxp,
                   args=[data[id]['kh'],fplnresults[id]['x1']])

    args = [data[id]['kh'],fplnresults[id]['x1']]
    fa = kfpln_ls(xa,*args)

    # try more searching with different algorithms, if necessary
    if fa > tolerance:
        print 'fa=',fa,' try Cauchy'
        xab,dab = anneal(kfpln_ls,xa,schedule='cauchy',
                         lower=minp,upper=maxp, 
                         args=[data[id]['kh'],fplnresults[id]['x1']])

        fab = kfpln_ls(xab,*args)
        if fab < fa:
            xa,fa,da = xab,fab,dab
        else:
            print 'fab=',fab,' try Boltzmann'
            xab,dab = anneal(kfpln_ls,p,schedule='boltzmann',
                             lower=minp,upper=maxp, 
                             args=[data[id]['kh'],fplnresults[id]['x1']])
            
            fab = kfpln_ls(xab,*args)
            if fab < fa:
                xa,fa,da = xab,fab,dab

    x,f,d = fmin_l_bfgs_b(kfpln_ls,xa,bounds=zip(minp,maxp), 
                          approx_grad=1,args=[data[id]['kh'],
                                              fplnresults[id]['x1']])

    print id,fa,f,x

    kfplnresults[id] = {'x':x,'obj':f,'info':d,'xa':xa,'obja':fa}


pickle.dump(kfplnresults, open('kfpln_fit_results_pickle','w'))
