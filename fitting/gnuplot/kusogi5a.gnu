set xrange [1e0:1e5];
set yrange [0.15:0.5];
unset format
#set format y "10^{%T}"
set format x "10^{%T}"
unset label;
unset logscale;
set logscale x; set xtics 1e-3,1e1,1e8
#set logscale y; set ytics 1e-12,1e1,1e6
set ytics 0.1,0.1,0.55;

set xlabel "h (cm)";
set ylabel "{/Symbol q}(h)";
r_m = -4.43551;
r_0 = -13.2677;
z_m = -11.2575;
s=2.33338;
k = 0.149;
s1=0.446;
s0=0.1066;
f(x) = x<(k/exp(r_m)) ? s1 : x>(k/exp(r_0)) ? s0 : s0+0.5*(s1-s0)*erfc((log(1/(1/x-exp(r_0)/k)-k/exp(r_m))-log(k)+z_m)/(s*sqrt(2)));

z_m2 = -8.24292;
sig=2.28541;
k = 0.149;
s1b=0.446;
s0b=0.00;
g(x) = s0b+0.5*(s1b-s0b)*erfc((log(x)-log(k)+z_m2)/(sig*sqrt(2)));

z_m3 = -8.24292;
sig2=2.28541;

h(x) = s0b+0.5*(s1b-s0b)*erfc((log(x)-log(k)+z_m3)/(sig2*sqrt(2)));
set label "(a)" at 10,0.3
set label "Beit Netofa clay" at 2,0.27

fit f(x) 'kosugi5mrc.dat' u 1:2 via s0,r_0,z_m
fit g(x) 'kosugi5mrc.dat' u 1:2 via z_m2
fit h(x) 'kosugi5mrc.dat' u 1:2 via z_m3,sig2

unset key
set key 120,0.22
set size 0.75,1.2
set output 'beitnetofa_mrc3-new.eps'
set term postscript eps enhanced 'Helvetica' 24 mono dashed
plot 'kosugi5mrc.dat' u 1:2 t 'data' w p pt 6 ps 2, h(x) t 'lognormal(1)' w l lw 2 lt 3, g(x) t 'lognormal(2)' w l lw 3 lt 2, f(x) t 'trunc. logn.' w l lw 3 lt 1
#
unset output