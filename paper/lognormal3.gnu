set xrange [1e1:1e3];
set yrange [1e-7:1e-3];
unset format
set format y "10^{%T}"
set format x "10^{%T}"
unset label;
unset logscale;
set logscale x; set xtics 1e1,1e1,1e3
set logscale y; set ytics 1e-7,1e1,1e-3

set xlabel "h (cm)";
set ylabel "g(h)" 2,0;
r_m = 1e-2;
r_g = 1.25e-3;
s=1.0;
k = 0.149;
f(x) = (1/(s*sqrt(2*pi)*(x-k/r_m)))*exp(-0.5*((log(x-k/r_m)-log(k/r_g))/s)**2);
g(x)=f(x)/x;
h(x)=f(x)/(x-k/r_m);

unset key
set key spacing 1.5
set size 1,1.2
set output 'mualem2.eps'
set term postscript eps enhanced 'Helvetica' 28 mono dashed
plot g(x) t 'f_H(h)/h' w l lt 6 lw 3, h(x) t 'f_H(h)/(h-h_c)' w l lt 1 lw 3