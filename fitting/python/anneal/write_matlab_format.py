# $Id: write_matlab_format.py,v 1.1 2008/06/12 23:48:04 kris Exp kris $

import pickle

# slurp files into lists of lines (headers and footers already removed)
ssclines = open('SSCBDKs_235.txt','r').readlines()
mrclines = open('Theta_h_2134.txt','r').readlines()

lnkresults = pickle.load(open('kfpln_fit_results_pickle','r'))
lnresults = pickle.load(open('fpln_fit_results_pickle','r'))

lnkscemresults = pickle.load(open('kfpln_scem_fit_results_pickle','r'))

# database of data is a dictionary
data = {}

# first column of ssc datafile will be dictionary key
for line in ssclines:
    key = line.split()[0]
    # each record is a dictionary

    # save USDA textures as an integer
    texsub = line.split()[8:20].index('1')
    tex = line.split()[20:24].index('1')
        
    rec = {'id':key,
           'ssc': tuple(float(x) for x in line.split()[2:5]),
           'bd':  float(line.split()[5]),
           'ks1': float(line.split()[6]),
           'ks2': float(line.split()[7]),
           'USDAsub': texsub, 'USDA': tex}
    data[key] = rec

fout = open('Theta_h_235.txt','w')

for line in mrclines:
    # only operate on the 235 soils in other 2 files
    if line.split()[0] in data:
        fout.write(line)
        
fout.close()


# write id, texture and fitted lnmodel parameters
fout = open('fitting_results_for_matlab.dat','w')

for id in data:
    output = [id,data[id]['USDA'],data[id]['USDAsub']]
    output.extend(lnresults[id]['x1'].tolist())   # th_mult, th_s, sig, mu
    output[3] = str(output[3]*output[4])               # th_r
    output.append(10.0**lnkscemresults[id]['xa'][0])   # K0
    output.append(lnkscemresults[id]['xa'][1])         # L
    output.append('\n') 
    output = [str(x) for x in output]
    fout.write(' '.join(output))

fout.close()
