function [ci] = PostProc(P,SCEMPar,pc);
% Derives the parameter combinations that belong to 100 * pc confidence interval
%
% Input         P           -- output from SCEM-UA -- stationary distribution
%               SCEMPar     -- SCEM-UA structure with information
%               pc          -- confidence interval, e.g. 0.68 ; 0.95 ; 0.99
%
%               ci          -- confidence intervals for parameters
%
% Written by   Jasper A. Vrugt,
%              Center for Nonlinear Studies
%              Los Alamos National Laboratory
%              January 2008
% ---------------------------------------------------------------------------------

% First determine the number of rows of ParSet
nP = size(P,1);

% Then Determine the signifance value
alpha = (1 - pc)/2;

% First straightforward approach based on probability mass -- works well with large n
for qq = 1:SCEMPar.n,
    p = sort(P(:,qq)); ci(qq,1) = p(round(alpha*nP),1); ci(qq,2) = p(round((1-alpha)*nP),1);
end;