set xrange [1e0:1e4];
set yrange [1e-5:3e0];
unset format
set format y "10^{%T}"
set format x "10^{%T}"
unset label;
unset logscale;
set logscale x; set xtics 1e-3,1e1,1e5
set logscale y; set ytics 1e-12,1e1,1e6
#set ytics 0.1,0.05,0.3;

set xlabel "h (cm)";
set ylabel "K_r(h)"; # 2,0;
r_m = -4.78338;
r_0 = -31.2222;
z_m = -7.85594;
s=1.1086;
k = 0.149;
f(x) = x<(k/exp(r_m)) ? 1.0 : x>(k/exp(r_0)) ? 1/0 :  0.5*erfc((log(1/(1/x-exp(r_0)/k)-k/exp(r_m))-log(k)+z_m)/(s*sqrt(2)));
g(x) = x<(k/exp(r_m)) ? 1.0 : x>(k/exp(r_0)) ? 1/0 : sqrt(f(x))*((0.5*erfc((log(1/(1/x-exp(r_0)/k)-k/exp(r_m))-log(k)+z_m+s**2)/(s*sqrt(2))))**2);

set label "(b)" at 2e1,2e-4
unset key
set key at 5e1,5e-5
set size 0.75,1.2
set output 'siltloam_2b.eps'
set term postscript eps enhanced 'Helvetica' 24 mono dashed
#fit g(x) 'kosugiK3.dat' u 1:2 via z_m,s,r_m
plot 'siltloamge3K.dat' using 1:2 title 'data' with p pt 6 ps 2, \
     g(x) title 'pred analytic' with l lw 4 lt 1, \
     'silt_loam_ge3_4param_numerically_integrated.dat' using 1:2 title 'pred numeric' with l lw 4 lt 2     
