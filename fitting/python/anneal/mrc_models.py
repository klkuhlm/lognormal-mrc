# $Id: mrc_models.py,v 1.6 2008/06/17 23:24:02 kris Exp kris $

# traditional van Genuchten model
def vgt(p,h):
    thr,ths,alpha,n = p[0:4]
    
    # model-predicted water content
    return (ths-thr)*(1.0 + (alpha*h)**n)**(1.0/n - 1.0) + thr

# four-parameter lognormal model
def fourpln(p,h):
    from scipy.special import erfc 
    from math import sqrt
    from numpy import zeros,array,log,concatenate

    kap = 0.149 # cm^2
    
    thr,ths,r0,rmax,sigz,muz = p[0:6]

    # check for physical parameter bounds
    if thr<0.0 or thr>=ths or ths>=1.0 or r0<0.0 or r0>=rmax or sigz<=0.0:
        return zeros((len(h),)) - 1.0  # return -1
    else:
        hmax = kap/r0
        hc = kap/rmax

        rt2z = sqrt(2.0)*sigz
        mueta = log(kap) - muz
        
        tlow = [ths for hval in h if hval < hc]
        thi = [thr for hval in h if hval > hmax]

        # assume h is monotonically increasing
        lo = len(tlow)
        hi = len(h) - len(thi)

        tmid = (ths-thr)*0.5*erfc((log(1.0/(1.0/h[lo:hi] - 1.0/hmax) - hc) - mueta)/rt2z) + thr

        return concatenate([tlow,tmid,thi])


