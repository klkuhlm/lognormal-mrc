function [p,log_p] = CompDensity(x,MCMCPar,Measurement,ModelName,Extra,option)
% This function computes the density of each x value

p = []; log_p = [];

if option == 6 % Model provides SSR - Write mcmod files - parallel sampler runs
    for ii = 1:size(x,1),
	WriteMcmodInp(x(ii,1:end),strcat( Extra.CaseName , '.' , num2str(ii) , '-mcmod.inp'), strcat( Extra.CaseName, '.' , num2str(ii)) );
    end;

    evalstr = ['mprun ./run-sampler.sh ', Extra.CaseName , ' 1 ', num2str( size(x,1) ), ' > /dev/null' ]; system(evalstr);
    id = sprintf( "%d", time ); 
end;

% Loop over the individual parameter combinations of x
for ii = 1:size(x,1),
    % Call model to generate simulated data
    if option < 6,
    	evalstr = ['ModPred = ',ModelName,'(x(ii,:),Extra);']; eval(evalstr);
    end;
    
    if option == 1, % Model directly computes posterior density
        p(ii,1:2) = [ModPred ii]; log_p(ii,1) = log(p(ii,1));
    end;

    if option == 2, % Model computes output simulation
        Err = (Measurement.MeasData(:)-ModPred(:));
        % Compute the number of measurement data
        N = size(Measurement.MeasData,1);
        % Derive the log likelihood
        log_p(ii,1) = N.*log(MCMCPar.Wb./Measurement.Sigma) - MCMCPar.Cb.*(sum((abs(Err./Measurement.Sigma)).^(2/(1+MCMCPar.Gamma))));        
        % And retain in memory
        p(ii,1:2) = [log_p(ii,1) ii]; 
    end;

    if option == 3, % Model computes output simulation
        Err = (Measurement.MeasData(:)-ModPred(:));
        % Derive the sum of squared error
        SSR = sum(abs(Err).^(2/(1+MCMCPar.Gamma)));
        % And retain in memory
        p(ii,1:2) = [-SSR ii]; log_p(ii,1) = -0.5 * SSR;
    end;

    if option == 4, % Model directly computes log posterior density
        p(ii,1:2) = [ModPred ii]; log_p(ii,1) = p(ii,1);
    end;

    if option == 5, % Similar as 3, but now weights with the Measurement Sigma
        % Defime the error
        Err = (Measurement.MeasData(:)-ModPred(:));
        % Derive the sum of squared error
        SSR = sum(abs(Err).^(2/(1+MCMCPar.Gamma)));
        % And retain in memory
        p(ii,1:2) = [-SSR ii]; log_p(ii,1) = -0.5 * SSR;
    end;

    if option == 6 % Model provides SSR
	evalstr = [ 'grep FINAL ' Extra.CaseName '.' num2str(ii)  ".out | awk '{print $5}'" ]; 
        [status SSRo] = system(evalstr,1);
	SSR = 1 - str2num(SSRo);
	p(ii,1:2) = [-SSR ii]; log_p(ii,1) = -0.5 * SSR;
	evalstr = [ 'cp -f ' Extra.CaseName '.' num2str(ii) ".out " Extra.CaseName '.' num2str(ii) '-' id ".out" ]; system( evalstr );
    end;

    if option == 7 % Model provides SSR - git-geosampler mpi version, sampler is parallelized
	WriteMcmodInp(x(ii,1:end),strcat( Extra.CaseName , '-mcmod.inp'), Extra.CaseName );
    	evalstr = ['./run-sampler.sh ', Extra.CaseName , ' > /dev/null' ]; system(evalstr);
	evalstr = [ 'grep FINAL ' Extra.CaseName ".rec | awk '{printf( \"%.2lf\", $5 )}'" ]; 
        [status SSRo] = system(evalstr,1);
	SSR = 1 - str2num(SSRo);
	p(ii,1:2) = [-SSR ii]; log_p(ii,1) = -0.5 * SSR;
        %id = sprintf( "%d", time ); 
	%evalstr = [ 'cp -f ' Extra.CaseName ".rec " Extra.CaseName '-' id ".rec" ]; system( evalstr );
    end;

    if option == 8 % Model provides SSR - git-geosampler mpi version, sampler is parallelized
	WriteGstatInp(x(ii,1:end),strcat( Extra.CaseName , '-gst.tpl'), Extra.CaseName );
    	evalstr = ['./run-sampler.sh ', Extra.CaseName , ' > /dev/null' ]; system(evalstr);
	evalstr = [ 'grep FINAL ' Extra.CaseName ".rec | awk '{print $5}'" ]; 
        [status SSRo] = system(evalstr,1);
	SSRo = str2num( SSRo );
	over = round( 100*mod( SSRo, 0.05 ))
	if over == 0
		SSRo = round( SSRo/0.05 ) * 0.05;
	elseif over <= 2
		SSRo = floor( SSRo/0.05 ) * 0.05;
	else
		SSRo = ceil( SSRo/0.05 ) * 0.05;
	end;
	SSR = 1 - SSRo;
	p(ii,1:2) = [-SSR ii]; log_p(ii,1) = -0.5 * SSR;
        %id = sprintf( "%d", time ); 
	%evalstr = [ 'cp -f ' Extra.CaseName ".rec " Extra.CaseName '-' id ".rec" ]; system( evalstr );
    end;

end;


