# $Id: class_fit.py,v 1.2 2008/06/11 23:51:33 kris Exp kris $

import pickle
import kh_models 
import mrc_models
from numpy import array,log10,nan_to_num,sum,zeros
from scipy.optimize import anneal,fmin_l_bfgs_b

data = pickle.load(open('mrc_kh_pickle','r'))
lnresults = pickle.load(open('fpln_fit_results_pickle','r'))
lnkresults = pickle.load(open('kfpln_fit_results_pickle','r'))

# labels for USDA texture classes **where there are actual members**
USDA = [['sand','loam','silt','clay'],
        ['sand','loamy sand','sandy loam','sandy clay loam',
           'clay','clay loam','loam','silty loam',
           'silt','silty loam','silty clay']]
name = ['USDA','USDAsub']

def fpln_ls_class(pin, *args):

    total = 0.0;

    for id in data:
        if data[id][args[1]] == args[0]:

            kh = array(data[id]['kh']['h']) 
            k = array(data[id]['kh']['k']) 
            
            t = array(data[id]['mrc']['t'])
            th = array(data[id]['mrc']['h'])

            pout = zeros((8,))

            # thrmult*ths = thr
            pout[0] = lnresults[id]['x1'][0]*lnresults[id]['x1'][1]
            pout[1] = lnresults[id]['x1'][1] # ths
            pout[2] = 10.0**(pin[1]+pin[0]) # r0_mult
            pout[3] = 10.0**pin[1] # rmax
            pout[4] = lnresults[id]['x1'][2] # sig_Z
            pout[5] = lnresults[id]['x1'][3] # mu_Z
            pout[6] = 10.0**(lnkresults[id]['x'][2]) # ksat
            pout[7] = lnkresults[id]['x'][3] # L
            
            # try to weight soils (rather than observations) equally
            kmod = kh_models.fourplnk(pout,kh)
            kmodlog = log10(nan_to_num(kmod))
            kn = len(kh)
            kres =  sum((kmodlog - log10(k))**2)/kn
            mrcres = sum((nan_to_num(mrc_models.fourpln(pout[0:6],th)) - t)**2)/len(th)

            total += (kres + mrcres)

    return total

# results
rclassresults = {}

minp = [-40.0,  -4.0]
maxp = [ -1.0,  20.0]
p =    [-40.0,  20.0]


for fam in range(len(name)):
    rclassresults[name[fam]] = {}

    for tex in range(len(USDA[fam])):

        xa,da = anneal(fpln_ls_class,p,lower=minp,upper=maxp,args=[tex,name[fam]])

        x1,f1,d1 = fmin_l_bfgs_b(fpln_ls_class,xa,approx_grad=True,
                                 bounds=zip(minp,maxp),args=[tex,name[fam]])

        rclassresults[name[fam]][USDA[fam][tex]] = {'xa':xa,'da':da,'x':x1,'obj':f1}
        print fam, USDA[fam][tex], xa,x1,f1

#        rclassresults[name[fam]][USDA[fam][tex]] = {'x':x1,'obj':f1}
#        print fam, USDA[fam][tex], x1,f1


pickle.dump(rclassresults, open('class_fit_results_pickle','w'))
