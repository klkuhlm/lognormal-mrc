hi = SCEMPar.ndraw;
lo = 0.25*hi;

nt = Extra.nt;

USDA = {'sand' 'loamy sand' 'sandy loam' 'sandy clay loam' ...
       'clay' 'clay loam' 'loam' 'silty loam' 'silt' 'silty clay loam' ...
       'silty clay'};
USDA2 = {'sand' 'loam' 'silt' 'clay'};

if SCEMPar.n == 8
    head = '_joint_';
else
    head = '_mrc_only_';
end

% color line based on relative sosr associated with that solution
s = SCEMPar.n+1;
sosrmin = min(abs(ParSet(lo:hi,s)));
sosrmax = max(abs(ParSet(lo:hi,s)));
sosrrange = sosrmax - sosrmin;

hvec = logspace(log10(min(Extra.htdat))-0.75,log10(max(Extra.htdat))+0.75,30);

p = [ParSet(lo,1:2),10.0.^ParSet(lo,3:4),ParSet(lo,5:6)];
tm = mrc_model(p,hvec);

figure(5);
semilogy(tm,hvec,'Color',ones(3,1)*(abs(ParSet(lo,s)) - sosrmin)/sosrrange);
xlabel('\theta')
ylabel('h')

hold on
for i=lo+1:hi
  p = [ParSet(i,1:2),10.0.^ParSet(i,3:4),ParSet(i,5:6)];
  semilogy(mrc_model(p,hvec),hvec,'Color',ones(3,1)*(abs(ParSet(i,s)) - sosrmin)/sosrrange);
end

% plot data as big red squares
semilogy(tdat(Extra.soil,1:Extra.nt),Extra.htdat,'ks','MarkerSize',10,'MarkerFaceColor','r');

% eps files are HUGE (10,000 overlapping curves); use png
print('-dpng',['mrc_results_',USDA2{Extra.class2+1},head,Extra.id,'.png']);
hold off;
