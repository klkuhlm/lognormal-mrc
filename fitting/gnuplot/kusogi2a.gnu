set xrange [1e0:1e4];
set yrange [0.1:0.6];
unset format
#set format y "10^{%T}"
set format x "10^{%T}"
unset label;
unset logscale;
set logscale x; set xtics 1e-3,1e1,1e5
#set logscale y; set ytics 1e-12,1e1,1e6
set ytics 0.1,0.1,0.6;

set xlabel "h (cm)";
set ylabel "{/Symbol q}(h)";
r_m = -6.263;
r_0 = -12.4107;
z_m = -6.763;
s=0.205903;
k = 0.149;
s1=0.469;
s0=0.190;
f(x) =  x< (k/exp(r_m)) ? s1 : x>(k/exp(r_0)) ? s0 : s0+0.5*(s1-s0)*erfc((log(1/(1/x-exp(r_0)/k)-k/exp(r_m))-log(k)+z_m)/(s*sqrt(2)));
f2(x) = x< (k/exp(r_m)) ? s1 : x>(k/exp(r_0)) ? s0 : s0+0.5*(s1-s0)*erfc((log(1/(1/x-exp(r_0)/k)-k/exp(r_m))-log(k)+z_m)/(s*sqrt(2)));

set label "(a)" at 2,0.2
set label "Touchet silt loam" at 2,0.17
set label "G.E. 3" at 2,0.145
unset key
set key
set size 0.75,1.2
set output 'touchet_sloam_1.eps'
set term postscript eps enhanced 'Helvetica' 24 mono dashed
fit f(x) 'kosugi2mrc.dat' u 1:2 via z_m,s,r_m,r_0
plot 'kosugi2mrc.dat' u 1:2 t 'data' w p pt 6 ps 2,f2(x) t 'fitted' w l lw 4 lt 1