function [S] = compare_fits(par,extra)
   
    nt = extra.nt;
        
    p = zeros(1,8);
    p(1) = par(1);  % thr
    p(2) = par(2);  % ths
    p(3) = 10.0^(par(3));  % r0
    p(4) = 10.0^(par(4));  % rmax
    p(5) = par(5);  % sigZ
    p(6) = par(6);  % muZ
    if extra.n == 8
        nk = extra.nk;
        p(7) = 10.0^(par(7));  % K0
        p(8) = par(8);  % L
    end
    
    tmod(1:nt) = mrc_model(p(1:6),extra.htdat(1:nt));
    
    if extra.n == 8
        kmod(1:nk) = kh_model(p,extra.hkdat(1:nk));
        S = [log10(kmod(1:nk)), tmod(1:nt)*extra.mult];
    elseif extra.n == 6
        S = tmod(1:nt);
    end

    
    
    

