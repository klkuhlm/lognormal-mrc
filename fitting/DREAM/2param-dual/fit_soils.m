function [modtheta] = fit_soils(par,extra)

  p = par;
  
  modtheta = mrc_2param_model(p,extra.ht);
  
