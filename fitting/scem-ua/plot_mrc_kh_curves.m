function plot_mrc_kh_curves(ParSet,SCEMPar,Extra,kdat,tdat)
    
    hi = SCEMPar.ndraw;
    lo = 0.25*hi;
    num = hi-lo+1;
    
    USDA = {'sand' 'loamy sand' 'sandy loam' 'sandy clay loam' ...
            'clay' 'clay loam' 'loam' 'silty loam' 'silt' 'silty clay loam' ...
            'silty clay'};
    USDA2 = {'sand' 'loam' 'silt' 'clay'};
    
    % color line based on relative sor associated with that solution
    s = SCEMPar.n+1;
    logsormin = log10(min(abs(ParSet(lo:hi,s))));
    logsormax = log10(max(abs(ParSet(lo:hi,s))));
    logsorrange = logsormax - logsormin;
    
    % compute parameter vector for all realizations
    p(1:num,1:8) = [ParSet(lo:hi,1:2),10.0.^ParSet(lo:hi,3:4), ...
                    ParSet(lo:hi,5:6),10.0.^ParSet(lo:hi,7),ParSet(lo:hi,8)];
    
    htvec = logspace(log10(min(Extra.htdat)) - 0.75, ...
                     log10(max(Extra.htdat)) + 0.75,30);
    tm = mrc_model(p(1,1:6),htvec);
    
    hkvec = logspace(log10(min(Extra.hkdat)) - 0.75, ...
                     log10(max(Extra.hkdat)) + 0.75,30);
    km = kh_model(p(1,1:8),hkvec);
    
    cv(1:num,1:3) = (log10(abs(ParSet(lo:hi,s*ones(3,1)))) - logsormin) ...
        /logsorrange;
    
    figure(3);
    subplot(1,2,1);
    semilogy(tm,htvec, 'Color',cv(1,1:3));
    xlabel('\theta')
    ylabel('h(\theta)')
    hold on
    
    subplot(1,2,2);
    loglog(hkvec,km, 'Color',cv(1,1:3));
    xlabel('h')
    ylabel('K(h)')
    hold on
    
    for i=lo+1:hi
        subplot(1,2,1);
        semilogy(mrc_model(p(i-lo,1:6),htvec),htvec,'Color',cv(i-lo,1:3));
        
        subplot(1,2,2);
        loglog(hkvec,kh_model(p(i-lo,:),hkvec),'Color',cv(i-lo,1:3));
    end
    
    % plot mrc data as big red squares
    subplot(1,2,1);
    semilogy(tdat(Extra.soil,1:Extra.nt),Extra.htdat,'rs','MarkerSize', ...
             10,'MarkerFaceColor','r');
    
    % plot mrc data as big blue squares (limit autoscaling on K)
    subplot(1,2,2);
    v = axis;
    axis([v(1:2),min(kdat(Extra.soil,1:Extra.nk))/10.0, ...
          max(kdat(Extra.soil,1:Extra.nk))*10.0]);
    loglog(Extra.hkdat,kdat(Extra.soil,1:Extra.nk),'bs','MarkerSize', ...
           10,'MarkerFaceColor','b');
    
    % eps files are HUGE (10,000+ overlapping curves);
    % but png fonts are broken?
    print('-dpng',['joint_results_',USDA2{Extra.class2+1}, ...
		    '_',Extra.id,'.png']);
    clf(3)
