clear all; clc;

SCEMPar.n = 22; 		% Dimension of the problem (Nr. parameters to be optimized in the model)
SCEMPar.q = 10;  	        % Number of complexes
SCEMPar.s = 2000;            % Number of random samples
SCEMPar.ndraw = 20000;      % Total number of function evaluations
SCEMPar.Gamma = 0;          % Kurtosis parameter Bayesian Inference Scheme

% Give the parameter ranges (minimum and maximum values)

% x and y for each circle
ParRange.minn = [-20.0*ones(1,11), -8.0*ones(1,11)];
ParRange.maxn = [  8.0*ones(1,11), 20.0*ones(1,11)];

% --------- Read in the "true" results -------------------------
ntot = 6188;   % number of data points

% loads variable 'true' (saved previously)
load true.mat

kobs = true(1:4117);
tobs = true(4118:6188);
true = [log10(kobs); tobs]; 

% Define the measurement data and sigma (non-informative prior)
Measurement.MeasData = true;
Measurement.Sigma = [];
% ---------------------------------------------------------------

% Define Extra
load hkdat.mat
load htdat.mat
load params.mat
Extra.hkdat = hkdat;
Extra.htdat = htdat;
Extra.params = params;
Extra.Jump = (2.4/sqrt(SCEMPar.n))^2; Extra.T = 1e7;

% Define the model to run
ModelName = 'fit_classes';

% Compute posterior density assuming noninformative prior (see Eq. [8])
option = 3;

% Run the SCEM_UA algorithm
[ParSet,GR,Sequences] = scem_ua(SCEMPar,ParRange,Measurement,ModelName,Extra,option);

% Derive the 95% confidence interval (pc = 0.95)
[ci_SCEM] = PostProc(ParSet(0.5*SCEMPar.ndraw:SCEMPar.ndraw,1:SCEMPar.n),SCEMPar,0.95);

% Write to screen
ci_SCEM

save results_080613d.mat
