nclass = 11;
lower1 = -20.0;
upper1 = 8.0;

lower2 = -8.0;
upper2 = 20.0;

nbins = 30;

r_image = zeros(nbins,nbins,nclass);

range1 = upper1 - lower1;
dx = range1/nbins;

for samp = 1:SCEMPar.ndraw - SCEMPar.s
    for soil=1:nclass
        i = floor((ParSet(SCEMPar.s + samp,soil) - lower1)/dx) + 1;  % r0
        j = floor((ParSet(SCEMPar.s + samp,nclass+soil) - lower2)/dx + 1);  % rmax
        r_image(j,i,soil) = r_image(j,i,soil) + 1;
    end
end

n = [51,19,24,6,14,6,35,56,2,13,9];
USDA = {'sand' 'loamy sand' 'sandy loam' 'sandy clay loam' ...
       'clay' 'clay loam' 'loam' 'silty loam' 'silt' 'silty clay loam' ...
       'silty clay'};
%n = [100,41,58,36];
%USDA = {'sand' 'loam' 'silt' 'clay'};

for soil = 1:nclass
    figure();
    imagesc(r_image(:,:,soil));
    colormap(1-gray);
    daspect([1 1 1])
   title(['a posteriori density ',USDA{soil},' n=',num2str(n(soil))]);
   set(gca,'XTick',linspace(1,nbins,(upper1-lower1)/4 + 1))
   set(gca,'XTickLabel','-20|-16|-12|-8|-4|0|4|8')
   set(gca,'YTick',linspace(1,nbins,(upper2-lower2)/4 + 1))
   set(gca,'YTickLabel','-8|-4|0|4|8|12|16|20')
    xlabel('log_{10}(r_0)')
    ylabel('log_{10}(r_{max})')
    print('-deps2', ['r_density_',sprintf('%02.2d',soil),'_',USDA{soil},'.eps']);
%%   print('-deps2', ['r_density_',num2str(soil),'_',USDA{soil},'.eps']);
end
