set xrange [1e1:1e4];
set yrange [1e-4:3e0];
unset format
set format y "10^{%T}"
set format x "10^{%T}"
unset label;
unset logscale;
set logscale x; set xtics 1e-3,1e1,1e5
set logscale y; set ytics 1e-12,1e1,1e6
#set ytics 0.1,0.05,0.3;

set xlabel "h (cm)";
set ylabel "K_r(h)"; # 2,0;
r_m = -5.98167;
r_0 = -9.14287;
z_m = -6.30024;
s=0.337319;
k = 0.149;

f(x) = x>(k/exp(r_m)) ?              0.5*erfc((log(1/(1/x-exp(r_0)/k)-k/exp(r_m))-log(k)+z_m)/(s*sqrt(2))) : 1;
g(x) = x>(k/exp(r_m)) ? sqrt(f(x))*((0.5*erfc((log(1/(1/x-exp(r_0)/k)-k/exp(r_m))-log(k)+z_m+s**2)/(s*sqrt(2))))**2) : 1;

set label "(b)" at 1e3,1e-3
unset key
set key at 1e4,2e0
set size 0.75,1.2
set output 'hygsandstone_2b.eps'
set term postscript eps enhanced 'Helvetica' 24 mono dashed
#fit g(x) 'kosugiK1.dat' u 1:2 via z_m,s
plot 'hygieneK.dat' using 1:2 t 'data' with p pt 6 ps 2, \
     g(x) title 'pred analytic' with l lw 4 lt 1, \
     'hygiene_sandstone_4param_numerically_integrated.dat' using 1:2 title 'pred numeric' with l lw 4 lt 2
