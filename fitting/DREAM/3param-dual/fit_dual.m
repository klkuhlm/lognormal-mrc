function [S] = fit_dual(par,extra)

  p = par(1:5);
  p(3) = 10.0^par(3);
  p(6) = 10.0^par(6); % K0

  modtheta = mrc_3param_model(p,extra.ht);
  modk      = kh_3param_model(p,extra.hk);

  S = [log10(modk),modtheta];