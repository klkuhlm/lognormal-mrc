set xrange [1e0:1e5];
set yrange [1e-5:3e0];
unset format
set format y "10^{%T}"
set format x "10^{%T}"
unset label;
unset logscale;
set logscale x; set xtics 1e-3,1e1,1e5
set logscale y; set ytics 1e-12,1e1,1e6
#set ytics 0.1,0.05,0.3;

set xlabel "h (cm)";
set ylabel "k_r(h)" 2,0;
r_m = -7.71283;
r_0 = -11.6379;
z_m = -11.79;
s=3.69134;
k = 0.149;

f(x) = x>(k/exp(r_m)) ? 0.5*erfc((log(1/(1/x-exp(r_0)/k)-k/exp(r_m))-log(k)+z_m)/(s*sqrt(2))) : 1;
g(x) = x>(k/exp(r_m)) ? sqrt(f(x))*((0.5*erfc((log(1/(1/x-exp(r_0)/k)-k/exp(r_m))-log(k)+z_m+s**2)/(s*sqrt(2))))**2) : 1;

z_m2 = -11.6797;
sig=2.872;

f2(x) = 0.5*erfc((log(x)-log(k)+z_m2)/(sig*sqrt(2)));
g2(x) = sqrt(f2(x))*((0.5*erfc((log(x)-log(k)+z_m2+sig**2)/(sig*sqrt(2))))**2);

z_m3 = -11.99;
sig2=2.87184;

f3(x) = 0.5*erfc((log(x)-log(k)+z_m3)/(sig2*sqrt(2)));
g3(x) = sqrt(f2(x))*((0.5*erfc((log(x)-log(k)+z_m3+sig2**2)/(sig2*sqrt(2))))**2);

#fit g(x) 'kosugiK5.dat' u 1:2 via z_m,s,r_m
fit g2(x) 'kosugiK5.dat' u 1:2 via sig

set label "(b)" at 1e1,1e-3
unset key
set key 150,1.0e-4
set size 0.75,1.2
set output 'beitnetofa_K3.eps'
set term postscript eps enhanced 'Helvetica' 24 mono dashed
plot 'kosugiK5.dat' u 1:2 t 'data' w p pt 6 ps 2, g3(x) t 'lognormal(1)' w l lw 2 lt 3, g2(x) t 'lognormal(2)' w l lw 2 lt 2, g(x) t 'trunc. logn.' w l lw 3 lt 1
#
unset output