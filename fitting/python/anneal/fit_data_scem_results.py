# $Id: fit_data_k4pln.py,v 1.2 2008/06/04 23:10:36 kris Exp kris $

import pickle
import kh_models
from numpy import *
from scipy.optimize import fmin_l_bfgs_b, anneal

data = pickle.load(open('mrc_kh_pickle','r'))
fplnresults = pickle.load(open('fpln_fit_results_pickle','r'))


# wrapper for calling 4-parameter ln routine
def kfpln_ls(pin, *args):
    h = array(args[0]['h']) 
    k = array(args[0]['k']) 

    pout = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    pout[0] = args[1][0]*args[1][1] # thrmult*ths = thr
    pout[1] = args[1][1] # ths
    pout[2] = args[2][0] # r0
    pout[3] = args[2][1] # rmax
    pout[4] = args[1][2] # sig_Z
    pout[5] = args[1][3] # mu_Z
    pout[6] = 10.0**pin[0] # ksat
    pout[7] = pin[1] # L

    return sum((log10(kh_models.fourplnk(pout,h)) - log10(k))**2)

# results
kfplnresults = {}
tolerance = 75.0

# loop over soils 
for id in fplnresults:
 
    # use r0 and rmax from fitting by textural classes using
    # scem-ua for mrc and k(h) jointly

    if data[id]['USDAsub'] == 0:
        r = [1.0E-12, 1.0E-5]
    elif data[id]['USDAsub'] == 1:
        r = [1.0E-12, 1.0E-5]
    elif data[id]['USDAsub'] == 2:
        r = [1.0E-10, 1.0E-6]
    elif data[id]['USDAsub'] == 3:
        r = [1.0E-7, 1.0E+5]
    elif data[id]['USDAsub'] == 4:
        r = [1.0E-10, 1.0E+3]
    elif data[id]['USDAsub'] == 5:
        r = [1.0E-15, 1.0E+17]
    elif data[id]['USDAsub'] == 6:
        r = [1.0E-14, 1.0E-7]
    elif data[id]['USDAsub'] == 7:
        r = [1.0E-16, 1.0E-7]
    elif data[id]['USDAsub'] == 8:
        r = [1.0E-12, 1.0E+12]
    elif data[id]['USDAsub'] == 9:
        r = [1.0E-18, 1.0E-6]
    else:
        r = [1.0E-6, 1.0]

    p = [log10(data[id]['ks2']),0.5]
    minp = [-4.0, -10.0]
    maxp = [ 7.0, +10.0]

    # use defaults for most cases
    xa,da = anneal(kfpln_ls,p,lower=minp,upper=maxp,
                   args=[data[id]['kh'],fplnresults[id]['x1'],r])

    args = [data[id]['kh'],fplnresults[id]['x1'],r]
    fa = kfpln_ls(xa,*args)

#    # try more searching with different algorithms, if necessary
#    if fa > tolerance:
#        print 'fa=',fa,' try Cauchy'
#        xab,dab = anneal(kfpln_ls,p,schedule='cauchy',dwell=1000,
#                         lower=minp,upper=maxp, 
#                         args=[data[id]['kh'],fplnresults[id]['x1']])
#
#        fab = kfpln_ls(xab,*args)
#        if fab < fa:
#            xa,fa,da = xab,fab,dab
#        else:
#            print 'fab=',fab,' try Boltzmann'
#            xab,dab = anneal(kfpln_ls,p,schedule='boltzmann',dwell=1000,
#                             lower=minp,upper=maxp,
#                             args=[data[id]['kh'],fplnresults[id]['x1']])
#            
#            fab = kfpln_ls(xab,*args)
#            if fab < fa:
#                xa,fa,da = xab,fab,dab

    x,f,d = fmin_l_bfgs_b(kfpln_ls,xa,bounds=zip(minp,maxp), 
                          approx_grad=1,args=[data[id]['kh'],
                                              fplnresults[id]['x1'],r])

    print id,fa,f,xa,x

    kfplnresults[id] = {'x':x,'obj':f,'info':d,'xa':xa,'obja':fa}


pickle.dump(kfplnresults, open('kfpln_scem_fit_results_pickle','w'))
