% ----------------- MATLAB implementation of the The Shuffled Complex Evolution Metropolis algorithm ----------------------
%
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
% Further reading: 
% 
% Vrugt J.A., H.V. Gupta, W. Bouten, S. Sorooshian, A Shuffled Complex Evolution Metropolis algorithm for optimization ...
%    and uncertainty assessment of hydrologic model parameters, Water Resour. Res., 39 (8), 1201, doi:10.1029/2002WR001642, 2003.
%
% Vrugt J.A., H.V. Gupta, L.A. Bastidas, W. Bouten, S. Sorooshian, Effective and efficient algorithm for multiobjective ...
%    optimization of hydrologic models, Water Resour. Res., 39 (8), 1214, doi:10.1029/2002WR001746, 2003.
%
% Vrugt J.A., S.C. Dekker, W. Bouten, Identification of rainfall interception model parameters from measurements of throughfall ...
%    and forest canopy storage, Water Resour. Res., 39 (9), 1251, doi:10.1029/2003WR002013, 2003.
%
% Vrugt J.A., G. Schoups, J.W. Hopmans, C. Young, W.W. Wallender, T. Harter, W. Bouten, Inverse modeling of large-scale spatially ...
%    distributed vadose zone properties using global optimization, Water Resour. Res., 40, W06503, doi:10.1029/2003WR002706, 2004.
%
% Vrugt J.A. W. Bouten, H.V. Gupta, and J.W. Hopmans, Toward improved identifiability of soil hydraulic parameters: on the ...
%    selection of a suitable parametric model, Vadose Zone Journal, 2, 98 - 113, 2003.
% 
%               Written by Jasper A. Vrugt, Tucson, AZ, July 2001 -- various modifications thereafter
%  
% = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
clear all; clc;

example = 1;

if example == 1, % LINEAR MODEL STRUCTURE -- Gaussian probability distribution

    SCEMPar.n = 2; 		% Dimension of the problem (Nr. parameters to be optimized in the model)
    SCEMPar.q = 10;  	        % Number of complexes
    SCEMPar.s = 100;            % Number of random samples
    SCEMPar.ndraw = 50000;      % Total number of function evaluations
    SCEMPar.Gamma = 0;          % Kurtosis parameter Bayesian Inference Scheme

    % Give the parameter ranges (minimum and maximum values)
    ParRange.minn = [-15 -15];
    ParRange.maxn = [ 15  15];

    % --------- Generate the synthetic data -- linear model --------- 
    % First x - value 
    x = 50 * unifrnd(-1,1,5000,1); 
    % Then the y - value with measurement sigma of 1
    y = x + normrnd(0,1,5000,1); 
    % How many data points are used in the analysis
    ntot = 10;
    % Define the measurement data and sigma (non-informative prior)
    Measurement.MeasData = y(1:ntot,1); Measurement.Sigma = [];
    % ---------------------------------------------------------------
    
    % Define Extra
    Extra.x = x(1:ntot,1); Extra.Jump = (2.4/sqrt(SCEMPar.n))^2; Extra.T = 1e7;
    % Define the model to run
    ModelName = 'Linmodel';

    % Compute posterior density assuming noninformative prior (see Eq. [8])
    option = 3; 
    
    % --------- Compare SCEM-UA with linear intervals ---------
    x0 = [1 1]; 
    % Define the input data
    Extra.data = Measurement.MeasData;
    % Then fit the linear model
    [xopt,rss] = fminsearch(@linmodel_sse,x0,[],Extra); yopt = linmodel(xopt,Extra);
    % Compute Jacobian -- dy/da and dy/db --> is 1 and x
    J = [ones(ntot,1) Extra.x];
    % And 95% parameter uncertainty bounds
    [ci_linear] = nlparci(xopt,yopt-Measurement.MeasData,J);
    % --------------------------------------------------------------------

    % Define sigma
    Measurement.Sigma = sqrt(rss/(ntot - SCEMPar.n));
    
    % Run the SCEM_UA algorithm
    [ParSet,GR,Sequences] = scem_ua(SCEMPar,ParRange,Measurement,ModelName,Extra,option);

    % Derive the 95% confidence interval (pc = 0.95)
    [ci_SCEM] = PostProc(ParSet(0.5*SCEMPar.ndraw:SCEMPar.ndraw,1:SCEMPar.n),SCEMPar,0.95);

    % Write to screen
    ci_linear , ci_SCEM
end;

if example == 2, % BANANA SHAPED POSTERIOR DISTRIBUTION

    SCEMPar.n = 2;                  % Dimension of the problem (Nr. parameters to be optimized in the model)
    SCEMPar.q = 5;                  % Number of complexes
    SCEMPar.s = 100;                % Number of random samples
    SCEMPar.ndraw = 20000;          % Total number of function evaluations
    SCEMPar.Gamma = 0;              % Kurtosis parameter Bayesian Inference Scheme

    % Give the parameter ranges (minimum and maximum values)
    ParRange.minn = [-30 -60 -5*ones(1,SCEMPar.n-2)];
    ParRange.maxn = [ 30  20  5*ones(1,SCEMPar.n-2)];

    % Define Extra
    Extra.C = zeros(SCEMPar.n,SCEMPar.n); Extra.C(1,1) = 100; loc = [2:SCEMPar.n]'; for zz = 1:length(loc), Extra.C(loc(zz),loc(zz)) = 1; end;
    Extra.b = [0.1]; Extra.detC = det(Extra.C); Extra.invC = inv(Extra.C); Extra.Const = 1/(sqrt((2*pi))*Extra.detC);

    % Define the Measurement Data
    Measurement.MeasData = []; Measurement.Sigma = [];
        
    % Define the model to run
    ModelName = 'Banshp';
    
    % Define some algorithmic variables
    Extra.Jump = (2.4/sqrt(SCEMPar.n))^2; Extra.T = 1e7;

    % Model output is directly posterior density
    option = 1; 

    % Run the SCEM_UA algorithm
    [ParSet,GR,Sequences] = scem_ua(SCEMPar,ParRange,Measurement,ModelName,Extra,option);
end;
