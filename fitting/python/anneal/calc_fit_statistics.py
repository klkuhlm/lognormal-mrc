# $Id: calc_fit_statistics.py,v 1.3 2008/06/17 19:54:43 kris Exp kris $

import pickle
from numpy import average,log10

data = pickle.load(open('mrc_kh_pickle','r'))
vgresults = pickle.load(open('vgt_fit_results_pickle','r'))
lnresults = pickle.load(open('fpln_fit_results_pickle','r'))
mvgresults = pickle.load(open('mvgt_fit_results_pickle','r'))
lnkresults = pickle.load(open('kfpln_fit_results_pickle','r'))


# compute and print average parameters for different textural classes
USDA = ['sand','loam','silt','clay']
USDAsub = ['sand','loamy sand','sandy loam','sandy clay loam',
           'sandy clay','clay','clay loam','loam',
           'silty loam','silt','silty clay loam','silty clay']

fout = open('mrc_vg_fit_stats02.csv','w')
f2out = open('mrc_ln_fit_stats02.csv','w')
fout.write( 'class, avg(thr), avg(ths), avg(alpha), avg(n), avg(Oh), sum(Oh), n \n')
f2out.write('class, avg(thr), avg(ths), avg(sigZ), avg(muZ), avg(Oh), sum(Oh), n \n')

kout = open('kh_vg_fit_stats02.csv','w')
k2out = open('kh_ln_fit_stats02.csv','w')
kout.write( 'class, avg(log10(K0)), avg(log10(Ksat)), avg(L), avg(Ok), sum(Ok), n \n')
k2out.write('class, avg(log10(K0)), avg(log10(Ksat)), avg(L), avg(log10(r0)),'+
            ' avg(log10(rmax)), avg(Ok), sum(Ok), n \n')

for tex in range(len(USDAsub)):
    vgthr = [];   vgths = []
    alpha = [];   vgn = []
    vgobj = [];   vgk0 = []
    vgL = [];     vgkobj = []

    lnthr = [];   lnths = []
    r0 = [];      rmax = []
    mu = [];      sig = []
    lnobj = [];   lnk0 = []
    lnkobj = [];  lnL = []
    kdat = []

    for id in vgresults:
        if data[id]['USDAsub'] == tex:
            kdat.append(data[id]['ks2'])

            vgthr.append(vgresults[id]['x'][0])
            vgths.append(vgresults[id]['x'][1])
            alpha.append(vgresults[id]['x'][2])
            vgn.append(vgresults[id]['x'][3])
            vgk0.append(mvgresults[id]['x'][0])
            vgL.append(mvgresults[id]['x'][1])
            vgobj.append(vgresults[id]['obj'])
            vgkobj.append(mvgresults[id]['obj'])
            
            lnthr.append(lnresults[id]['x1'][0]*lnresults[id]['x1'][1])
            lnths.append(lnresults[id]['x1'][1])
            r0.append(lnkresults[id]['x'][0] + lnkresults[id]['x'][1])
            rmax.append(lnkresults[id]['x'][1])
            sig.append(lnresults[id]['x1'][2])
            mu.append(lnresults[id]['x1'][3])
            lnobj.append(lnresults[id]['f1'])
            lnk0.append(lnkresults[id]['x'][2])
            lnL.append(lnkresults[id]['x'][3])
            lnkobj.append(lnkresults[id]['obj'])

    if len(vgthr) > 0:
        # vG mrc
        pstr = [ str(average(x)) for x in (vgthr,vgths,alpha,vgn,vgobj)]
        pstr.insert(0,USDAsub[tex])
        pstr.extend([str(sum(vgobj)),str(len(vgthr)) + ' \n'])
        fout.write(', '.join(pstr))

        # 4pln mrc
        pstr = [str(average(x)) for x in (lnthr,lnths,sig,mu,lnobj)]
        pstr.insert(0,USDAsub[tex])
        pstr.extend([str(sum(lnobj)),str(len(lnthr)) + ' \n'])
        f2out.write(', '.join(pstr))

        # mvG K(h)
        kdat = log10(kdat)
        pstr = [ str(average(x)) for x in (vgk0,kdat,vgL,vgkobj)]
        pstr.insert(0,USDAsub[tex])
        pstr.extend([str(sum(vgkobj)),str(len(vgk0)) + ' \n'])
        kout.write(', '.join(pstr))

        # 4pln K(h)
        pstr = [str(average(x)) for x in (lnk0,kdat,lnL,r0,rmax,lnkobj)]
        pstr.insert(0,USDAsub[tex])
        pstr.extend([str(sum(lnkobj)),str(len(lnk0)) + ' \n'])
        k2out.write(', '.join(pstr))

fout.close()
f2out.close()
kout.close()
k2out.close()
        
