set xrange [1e1:1e3];
set yrange [0.1:0.3];
unset format
#set format y "10^{%T}"
set format x "10^{%T}"
unset label;
unset logscale;
set logscale x; set xtics 1e-3,1e1,1e5
#set logscale y; set ytics 1e-12,1e1,1e6
set ytics 0.1,0.05,0.3;

set xlabel "h (cm)";
set ylabel "{/Symbol q}(h)";
r_m = -5.0;
r_0 = -8.5061;
z_m = -6.12819;
s=0.205903;
k = 0.149;
s1=0.25;
s0=0.153;
f(x) =s0+0.5*(s1-s0)*erfc((log(1/(1/x-exp(r_0)/k)-k/exp(r_m))-log(k)+z_m)/(s*sqrt(2)));
f2(x) = x>(k/exp(r_m)) ? s0+0.5*(s1-s0)*erfc((log(1/(1/x-exp(r_0)/k)-k/exp(r_m))-log(k)+z_m)/(s*sqrt(2))) : s1;
unset key

set label "(a)" at 15,0.15
set label "Hygiene sandstone" at 15,0.135
set key
set size 0.75,1.2
set output 'hygsandstone_1b.eps'
set term postscript eps enhanced 'Helvetica' 24 mono dashed
fit f(x) 'hygienemrc.dat' u 1:2 via r_m,r_0,z_m,s
plot 'hygienemrc.dat' u 1:2 t 'data' w p pt 6 ps 2,f2(x) t 'fitted' w l lw 4 lt 1