function k = kh_model(p,h)

  kap  = 0.149;
  thr  = p(1);
  ths  = p(2);
  r0   = p(3);
  rmax = p(4);
  sigz = p(5);
  muz  = p(6);
  K0   = p(7);
  L    = p(8);

  if r0 >= rmax || thr >= ths
    k = 1.0E-100*ones(1,length(h));
  else
    Se = (mrc_model(p(1:6),h) - thr)/(ths - thr);

    hmax = kap/r0;
    hc   = kap/rmax;
    mueta = log(kap) - muz;

    mink = 1.0E-100;
    rt2z = sqrt(2.0)*sigz;

    k = zeros(1,length(h));

    for i=1:length(h)
      if h(i) < hc
        k(i) = K0;
      elseif h(i) > hmax
        k(i) = mink;
      else
        u = 1.0/(1.0/h(i) - 1.0/hmax) - hc;
        k(i) = max( mink, K0*Se(i)^L* ...
                   (0.5*erfc((log(u) - (mueta - sigz^2))/rt2z))^2 );
      end
    end
  end
        
