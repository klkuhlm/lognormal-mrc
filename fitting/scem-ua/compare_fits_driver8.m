clear all; clc;

SCEMPar.n = 8; 		% Dimension of the problem (Nr. parameters
                        % to be optimized in the model)

SCEMPar.q = 10;  	        % Number of complexes
SCEMPar.s = 500;            % Number of random samples
SCEMPar.ndraw = 20000;      % Total number of function evaluations
SCEMPar.Gamma = 0;          % Kurtosis parameter Bayesian Inference Scheme

% Give the parameter ranges (minimum and maximum values)

if SCEMPar.n == 6 %  thr  ths   r0    rmax  sig   mu    
    ParRange.minn = [0.0, 0.0, -20.0, -6.0,  0.0, -20.0];
    ParRange.maxn = [0.4, 0.9,   6.0, 20.0  10.0,  2.0];
else                                                    %  K0      L
    ParRange.minn = [0.0, 0.0, -20.0, -6.0,  0.0, -20.0, -7.0, -10.0];
    ParRange.maxn = [0.4, 0.9,   6.0, 20.0  10.0,  2.0,   7.0,  10.0];
end

load tdat.mat
load kdat.mat
load hkdat.mat
load htdat.mat
load params.mat

Extra.Jump = (2.4/sqrt(SCEMPar.n))^2; Extra.T = 1e7;
ModelName = 'compare_fits';

% Compute posterior density assuming noninformative prior (see Eq. [8])
option = 3;


% there are 235 soils, loop over them, running scem-ua for each
for soil = 1:235
    Extra.id = num2str(params(soil,1));   % UNSODA id number
    Extra.class2 = params(soil,2);  % USDA class
    Extra.class = params(soil,3);   % USDA sub-class
    Extra.n = SCEMPar.n;
    Extra.soil = soil;

    nk = sum(~isnan(hkdat(soil,:)));  
    nt = sum(~isnan(htdat(soil,:)));  

    disp(num2str(soil));
    
    Extra.nk = nk;
    Extra.nt = nt;

    Extra.mult = 4.0*nk/nt;  % try to balance weights between objective fcns

    
    if SCEMPar.n == 8
      Measurement.MeasData = [log10(kdat(soil,1:nk)), ...
                              tdat(soil,1:nt)*Extra.mult];
    else
        Measurement.MeasData = tdat(soil,1:nt);
    end

    % Define the measurement data and sigma (non-informative prior)

    Measurement.Sigma = [];
    % ---------------------------------------------------------------

    % Define Extra
    Extra.hkdat = hkdat(soil,1:nk);
    Extra.htdat = htdat(soil,1:nt);

    clear ParSet GR Sequences;

    % Run the SCEM_UA algorithm
    [ParSet,GR,Sequences] = scem_ua(SCEMPar,ParRange,Measurement, ...
                                    ModelName,Extra,option);

    % Derive the 95% confidence interval (pc = 0.95)
    [ci_SCEM] = PostProc(ParSet(0.5*SCEMPar.ndraw:SCEMPar.ndraw,1: ...
                                SCEMPar.n),SCEMPar,0.95);
   
    save(['fit_results_',Extra.id,'.mat'])
    make_histograms(SCEMPar,Extra,ParSet);
    plot_mrc_kh_curves(ParSet,SCEMPar,Extra,kdat,tdat);
end

