function make_histograms(SCEMPar,Extra,ParSet)

par = {'\theta_r' '\theta_s' 'log_{10}(r_0)' 'log_{10}(r_{max})' ...
       '\sigma_Z' '\mu_Z' 'log_{10}(K_0)' 'L'};

npar = SCEMPar.n;
hi = SCEMPar.ndraw;
lo = hi*0.25;
soil = Extra.id;

if npar == 8
    head = '_joint_';
else
    head = '_mrc_only_';
end

USDA = {'sand' 'loamy sand' 'sandy loam' 'sandy clay loam' ...
        'clay' 'clay loam' 'loam' 'silty loam' 'silt' 'silty clay loam' ...
        'silty clay'};
USDA2 = {'sand' 'loam' 'silt' 'clay'};

figure(1);
for i = 1:npar
    if npar == 8
        subplot(2,4,i);
    else
        subplot(2,3,i);
    end
    hist(ParSet(lo:hi,i),50);
    xlabel(par{i});
    ylabel('frequency');
end
if npar == 8
    % printing figures from matlab ... source of much frustration.
    set(gcf,'Position',[0,0,1700,400],'PaperType','A3', ...
            'PaperPositionMode','auto') 
else
    set(gcf,'Position',[0,0,1300,400],'PaperType','A3', ...
            'PaperPositionMode','auto')
end
print('-depsc',['hist_',USDA2{Extra.class2+1},head,soil,'.eps']);
clf(1);

figure(2);
semilogy(ParSet(lo:hi,npar+1));
title(['SOR through iterations ',soil,' ',USDA{Extra.class}]);
xlabel('Iterations');
ylabel('Sum of residuals');
print('-depsc',['SOR_',USDA2{Extra.class2+1},head,soil,'.eps']);
clf(2);
