function [S] = fit_classes(par,extra)

% all matricies are sorted by id

% 9 textural classes, r0 and rmax for each

% columns of extra.params
%  1   2     3        4    5    6     7    8   9
%  id, USDA, USDAsub, thr, ths, sigZ, muZ, K0, L

r(1,1:11) = par(1:11);
r(2,1:11) = par(12:22);

% there are 235 soils
n = 235;
kmod = nan(n,89);  % 89 values maximum
tmod = nan(n,10);  % 10 values maximum

for i=1:n
    p = zeros(1,8);
    p(1) = extra.params(i,4);  % thr
    p(2) = extra.params(i,5);  % ths
    p(3) = 10.0^(r(1,extra.params(i,3)));  % r0
    p(4) = 10.0^(r(2,extra.params(i,3)));  % rmax
    p(5) = extra.params(i,6);  % sigZ
    p(6) = extra.params(i,7);  % muZ
    p(7) = extra.params(i,8);  % K0
    p(8) = extra.params(i,9);  % L
    
    knum = sum(~isnan(extra.hkdat(i,:)));
    tnum = sum(~isnan(extra.htdat(i,:)));
    kmod(i,1:knum) = kh_model(p,extra.hkdat(i,1:knum));
    tmod(i,1:tnum) = mrc_model(p(1:6),extra.htdat(i,1:tnum));
end

S = [log10(kmod(~isnan(kmod))); tmod(~isnan(tmod))];
    
    
    
    

