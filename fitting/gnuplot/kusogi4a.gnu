set xrange [1e0:1e4];
set yrange [0.0:0.4];
unset format
#set format y "10^{%T}"
set format x "10^{%T}"
unset label;
unset logscale;
set logscale x; set xtics 1e-3,1e1,1e5
#set logscale y; set ytics 1e-12,1e1,1e6
set ytics 0.1,0.1,0.6;

set xlabel "h (cm)";
set ylabel "{/Symbol q}(h)";
r_m = -3.0;
r_0 = -14.5061;
z_m = -7.967;
s=1.093;
k = 0.149;
s1=0.326;
s0=0.083;
f(x) = x>(k/exp(r_m)) ? s0+0.5*(s1-s0)*erfc((log(1/(1/x-exp(r_0)/k)-k/exp(r_m))-log(k)+z_m)/(s*sqrt(2))) : s1;
f2(x) = x>(k/exp(r_m)) ? s0+0.5*(s1-s0)*erfc((log(1/(1/x-exp(r_0)/k)-k/exp(r_m))-log(k)+z_m)/(s*sqrt(2))) : s1;

set label "(a)" at 2,0.1
set label "Gravelly sand" at 2,0.07
set label "G.E. 9" at 2,0.045
unset key
set key
set size 0.75,1.2
set output 'gravelsand_1.eps'
set term postscript eps enhanced 'Helvetica' 24 mono dashed
fit f(x) 'kosugi4mrc.dat' u 1:2 via r_0,r_m,z_m,s
plot 'kosugi4mrc.dat' u 1:2 t 'data' w p pt 6 ps 2,f2(x) t 'fitted' w l lw 4 lt 1