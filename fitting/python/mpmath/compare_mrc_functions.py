import mpmath as m
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import fmin_l_bfgs_b
from scipy.special import erfc

sqrt2 = m.sqrt(2.0)
sqrt2pi = m.sqrt(2.0*m.pi)
kappa = m.mpf('0.149')
lnkap = m.log(kappa)
Mone = m.mpf('1.0')
Mzed = m.mpf('0.0')

optimize = False
plotFigs = False

# ########################################
# 2-parameter H, MRC & K 
def fH2(h,sigz,muz):
    "Brutsaert 1966"
    
    mueta = lnkap - muz
    fH = m.exp(-m.power((m.log(h)-mueta)/(sigz*sqrt2),2))/(sqrt2pi*sigz*h)
    return fH

def MRC2(h,sigz,muz):
    "Kosugi 1996"

    mueta = lnkap - muz
    return m.erfc((m.log(h)-mueta)/(sqrt2*sigz))/2.0

def Kr2(theta,h,sigz,muz):
    "Kosugi 1996"

    mueta = lnkap - muz
    return m.sqrt(theta)*m.erfc((m.log(h)-(mueta-sigz**2))/(sqrt2*sigz))/2.0

# ########################################
# 3-parameter H, MRC & K
def fH3(h,sigz,muz,rmax):
    """three-parameter upper-truncated lognormal 
    PDF for capillary pressure head; Kosugi 1994 (eqn. 18)"""

    mueta = lnkap - muz
    hc = kappa/rmax
    
    if h > hc:
        fH = m.exp(-m.power((m.log(h-hc)-mueta)/sigz,2)/2.0)/(sqrt2pi*sigz*(h-hc))
    else:
        fH = Mzed
    return fH

def MRC3(h,sigz,muz,rmax):
    """ three-parameter upper-truncated lognormal
    moisture retention curve from Kosugi"""

    mueta = lnkap - muz
    hc = kappa/rmax

    if h > hc:
        theta = m.erfc((m.log(h-hc)-mueta)/(sqrt2*sigz))/2.0
    else: # h >= lbound
        theta = Mone
    return theta

def Kr3(theta,h,sigz,muz,rmax):
    """three-parameter upper-truncated lognormal relative
    hydraulic conductivity curve, using approximate formulation
    for Mualem relationship"""

    mueta = lnkap - muz
    hc = kappa/rmax

    if h>hc:
        Kr = m.sqrt(theta)*m.power(m.erfc((m.log(h-hc)-(mueta-sigz**2))/(sqrt2*sigz))/2.0,2)
    else:
        Kr = Mone
    return Kr

# ########################################
# 4-parameter H, MRC & K
def fH4(h,sigz,muz,rmax,r0):
    """four-parameter upper-truncated lognormal 
    PDF for capillary pressure head"""

    mueta = lnkap - muz
    hc = kappa/rmax
    hmax = kappa/r0
    u = 1/(1/h - 1/hmax) - hc

    if h>hc and h<hmax:
        fH = m.exp(-m.power((m.log(u)-mueta)/sigz,2)/2.0)/(sqrt2pi*sigz*u) 
    else:
        fH = Mzed
    return fH

def MRC4(h,sigz,muz,rmax,r0):
    """ four-parameter doubly-truncated lognormal
    moisture retention curve"""

    mueta = lnkap - muz
    hc = kappa/rmax
    hmax = kappa/r0

    if h>hc and h<hmax:
        u = 1/(1/h - 1/hmax) - hc
        theta = m.erfc((m.log(u)-mueta)/(sqrt2*sigz))/2.0
    elif h <= hc:
        theta = Mone
    else: # h >= hmax
        theta = Mzed
    return theta

def Kr4(theta,h,sigz,muz,rmax,r0):
    """four-parameter upper-truncated lognormal relative
    hydraulic conductivity curve, using approximate formulation
    for Mualem relationship"""

    mueta = lnkap - muz
    hc = kappa/rmax
    hmax = kappa/r0

    if h>hc and h<hmax:
        u = 1/(1/h - 1/hmax) - hc
        Kr = m.sqrt(theta)*m.power(m.erfc((m.log(u)-(mueta-sigz**2))/(sqrt2*sigz))/2.0,2)
    elif h <= hc:
        Kr = Mone
    else: # h >= hmax
        Kr = Mzed
    return Kr


# ########################################
def MualemNum(fH,thetastar,h,*args):
    """numerically integrate a moisture pdf using Mualem's rule
    written in terms of the PDF of pressure head. For comparing
    against approximate analytical method in paper."""
    
    if len(args) == 2:
        # 2-parameter model
        hc = 0.0
        hmax = m.inf
    if len(args) == 3:
        # 3-parameter model
        (sigz,muz,rmax) = args
        hc = kappa/rmax
        hmax = m.inf
    elif len(args) == 4:
        # 4-parameter model
        (sigz,muz,rmax,r0) = args
        hc = kappa/rmax
        hmax = kappa/r0

    f = lambda z : fH(z,*args)/z

    if h > hc and h < hmax:
        lo = m.quad(f,[0.0,h],maxdegree=10)
        hi = m.quad(f,[h,m.inf],maxdegree=10)
        return m.sqrt(thetastar)*m.power(hi/(lo+hi),2)
    elif h <= hc:
        return Mone
    else: # h >= hmax
        return Mzed

if __name__ == "__main__":
    
    alfs = 14  # font size
    figsz = (6.5,4)

    # Figures for paper
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    # define list of dictionaries of properties for soils
    soils = [{'prefix':'hygiene_sandstone',
              'mrcdat':'../../gnuplot/hygienemrc.dat',
              'krdat':'../../gnuplot/hygieneK.dat',
              'MRC4':{
                'r0':m.mpf('1.077E-4'), 'rm':m.mpf('2.52E-3'),
                'muz':m.mpf('-6.3'),  'sigz':m.mpf('0.337'),
                's0':m.mpf('0.153'),    's1':m.mpf('0.25')},
              'MRC3':{
                'rm':m.mpf('3.052E-3'),
                'muz':m.mpf('-6.3'),  'sigz':m.mpf('0.3361'),
                's0':m.mpf('0.1466'),   's1':m.mpf('0.2513')},
              'BOTH4':{
                'r0':m.mpf('0.0001077'), 'rm':m.mpf('0.00252'),
                'muz':m.mpf('-6.3'),   'sigz':m.mpf('0.337'),
                's0':m.mpf('0.153'),    's1':m.mpf('0.25'),
                'k0':m.mpf('1.0')},
              'BOTH3':{
                'rm':m.mpf('0.002652'), 
                'muz':m.mpf('-6.231'),   'sigz':m.mpf('0.2952'),
                's0':m.mpf('0.1478'),    's1':m.mpf('0.2472'),
                'k0':m.mpf('1.096')},
              'h':np.logspace(0.0,2.75,100),
              'xlim':([15.0,300.0],[15.0,300.0]),
              'ylim':([0.13,0.27],[5.0E-3,1.5])},
             {'prefix':'silt_loam_ge3',
              'mrcdat':'../../gnuplot/siltloamge3mrc.dat',
              'krdat':'../../gnuplot/siltloamge3K.dat',
              'MRC4':{
                'r0':m.mpf('1.475E-4'),  'rm':m.mpf('1.27E-2'),
                'muz':m.mpf('-7.927'), 'sigz':m.mpf('1.118'),
                's0':m.mpf('0.192'),     's1':m.mpf('0.395')},
              'MRC3':{
                'rm':m.mpf('0.03107'),
                'muz':m.mpf('-7.689'), 'sigz':m.mpf('0.8052'),
                's0':m.mpf('0.1705'),    's1':m.mpf('0.3935')},
              'BOTH4':{
                'r0':m.mpf('0.0001142'),  'rm':m.mpf('0.2124'),
                'muz':m.mpf('-7.971'), 'sigz':m.mpf('1.026'),
                's0':m.mpf('0.1804'),     's1':m.mpf('0.3892'),
                'k0':m.mpf('0.9671')},
              'BOTH3':{
                'rm':m.mpf('3.107'),
                'muz':m.mpf('-7.562'), 'sigz':m.mpf('0.8414'),
                's0':m.mpf('0.1782'),    's1':m.mpf('0.3981'),
                'k0':m.mpf('0.9242')},
              'h':np.logspace(0.0,3.5,100),
              'xlim':([3.0,3000.0],[3.0,3000.0]),
              'ylim':([0.13,0.42],[3.0E-3,1.5])},
             {'prefix':'beit_netofa_clay',
              'mrcdat':'../../gnuplot/beitnetofamrc.dat',
              'krdat':'../../gnuplot/beitnetofaK.dat',
              'MRC4':{
                'r0':m.mpf('4.36E-7'),   'rm':m.mpf('1.32E-2'),
                'muz':m.mpf('-11.06'), 'sigz':m.mpf('2.333'),
                's0':m.mpf('0.1'),       's1':m.mpf('0.45')},
              'MRC3':{
                'rm':m.mpf('1.239E-3'),
                'muz':m.mpf('-11.03'), 'sigz':m.mpf('2.568'),  
                's0':m.mpf('0.1194'),    's1':m.mpf('0.4437')},
              'BOTH4':{
                'r0':m.mpf('4.36e-07'), 'rm':m.mpf('0.0132'),
                'muz':m.mpf('-11.06'), 'sigz':m.mpf('2.333'),
                's0':m.mpf('0.1'),    's1':m.mpf('0.45'),
                'k0':m.mpf('1.00')},
              'BOTH3':{
                'rm':m.mpf('45.9'),
                'muz':m.mpf('-9.072'), 'sigz':m.mpf('1.665'),
                's0':m.mpf('0.2599'),    's1':m.mpf('0.4761'),
                'k0':m.mpf('0.9655')},
              'h':np.logspace(-0.5,4.5,100),
              'xlim':([10.0,3.0E+4],[0.5,1.0E+4]),
              'ylim':([0.2,0.47],[1.0E-3,1.5])}]
    
    if 0:
        sn = {'hygiene_sandstone':0, 'silt_loam_ge3':1, 'beit_netofa_clay':2}
    
        # update parameter database with output from optimization
        fh = open('optimization.out','r')
        lines = fh.readlines()
        for j,line in enumerate(lines):
            if 'soil:' in line[:5]:
                idx = sn[line.split(':')[1].strip()]  # soil name as index
                fit = lines[j+1].split(':')[1].strip()  # MRC only or MRC+Kr (BOTH), and number of parameters
                keys = [x.split('=')[0].strip() for x in lines[j+1].split()] # parameter names
                vals = [float(x.split('=')[1].strip()) for x in lines[j+1].split() if '=' in x]  # parameter values
                for k,v in zip(keys,vals):
                    soils[idx][fit][k] = v

        print 'silt MRC3 rm:',soils[1]['MRC3']['rm']
        print 'silt MRC4 rm:',soils[1]['MRC4']['rm']
    
    if plotFigs:
        # plot figures of fits using only MRC data
        for s in soils[0:2]:   # just sand and silt
            print 'MRC only'
            #for k,v in s.iteritems():
            #    print k,'::',v
            fig = plt.figure(1,figsize=figsz)
            axl = fig.add_subplot(121)
            mrcdata = np.loadtxt(s['mrcdat'])
            axl.semilogx(mrcdata[:,0],mrcdata[:,1],'ro')
            
            h = s['h']
            mrc = np.empty((h.shape[0]),'f')
            s4 = s['MRC4']
            
            props4 = (s4['sigz'],s4['muz'],s4['rm'],s4['r0'])
            mrc[:] = map(lambda x: MRC4(x,*props4),h)
            # convert from Se to theta for plotting
            mrc *= s4['s1']-s4['s0']
            mrc += s4['s0']
            axl.semilogx(h,mrc[:],'k-')
    
            s3 = s['MRC3']
            props3 = (s3['sigz'],s3['muz'],s3['rm'])
            mrc[:] = map(lambda x: MRC3(x,*props3),h)
            mrc *= s3['s1']-s3['s0']
            mrc += s3['s0']
            axl.semilogx(h,mrc[:],'r-')
    
            axl.set_xlabel('$h$ [cm]',fontsize=alfs)
            axl.set_ylabel('$\\theta(h)$',fontsize=alfs)
            axl.set_xlim(s['xlim'][0])
            axl.set_ylim(s['ylim'][0])
            axl.annotate('(a)',xy=(0.8,0.9),xycoords='axes fraction',size=14)

            axr = fig.add_subplot(122)
            krdata = np.loadtxt(s['krdat'])
            
            Ka = np.empty((h.shape[0],3)) # analytically integrated K
            Kn = np.empty((h.shape[0],3)) # numerically integrated K
            
            for i,val in enumerate(h):
                if i%20==0:
                    print i
    
                thetastar = MRC4(val,*props4)
                Ka[i,2] = Kr4(thetastar,val,*props4)
                Kn[i,2] = MualemNum(fH4,thetastar,val,*props4)
            
                thetastar = MRC3(val,*(props3))
                Ka[i,1] = Kr3(thetastar,val,*(props3))
                Kn[i,1] = MualemNum(fH3,thetastar,val,*(props3))
                
            axr.loglog(krdata[:,0],krdata[:,1],'ro',markerfacecolor='yellow')
            # 4-parameter
            axr.loglog(h,Kn[:,2],'k--') # numeric
            axr.loglog(h,Ka[:,2],'k-')  # analytical
            
            # 3-parameter
            axr.loglog(h,Kn[:,1],'r--')  # numeric
            axr.loglog(h,Ka[:,1],'r-')  # analytical
            
            axr.set_xlabel('$h$ [cm]',fontsize=alfs)
            axr.set_ylabel('$K_r(h)$',fontsize=alfs)
            axr.set_xlim(s['xlim'][1])
            axr.set_ylim(s['ylim'][1])
            #plt.subplots_adjust(wspace=0.4)
            axr.annotate('(b)',xy=(0.8,0.9),xycoords='axes fraction',size=14)
            plt.tight_layout()
            plt.savefig(s['prefix']+'_MRConly_figures.pdf')
            plt.savefig(s['prefix']+'_MRConly_figures.png')
            plt.close(1)
    
        # plot figures of fits to both MRC and K data
        for s in soils[2:3]:  # just clay
            print 'MRC and Kr'
            #for k,v in s.iteritems():
            #    print k,'::',v
            fig = plt.figure(1,figsize=figsz)
            axl = fig.add_subplot(121)
            mrcdata = np.loadtxt(s['mrcdat'])
            axl.semilogx(mrcdata[:,0],mrcdata[:,1],'ro')
            
            h = s['h']
            mrc = np.empty((h.shape[0]),'f')
            s4 = s['BOTH4']
            
            props4 = (s4['sigz'],s4['muz'],s4['rm'],s4['r0'])
            mrc[:] = map(lambda x: MRC4(x,*props4),h)
            # convert from Se to theta for plotting
            mrc *= s4['s1']-s4['s0']
            mrc += s4['s0']
            axl.semilogx(h,mrc[:],'k-') # 4-parameter MRC model
    
            s3 = s['BOTH3']
            props3 = (s3['sigz'],s3['muz'],s3['rm'])
            mrc[:] = map(lambda x: MRC3(x,*props3),h)
            mrc *= s3['s1']-s3['s0']
            mrc += s3['s0']
            axl.semilogx(h,mrc[:],'r-') # 3-parameter MRC model
    
            axl.set_xlabel('$h$ [cm]',fontsize=alfs)
            axl.set_ylabel('$\\theta(h)$',fontsize=alfs)
            axl.set_xlim(s['xlim'][0])
            axl.set_ylim(s['ylim'][0])
            axl.annotate('(a)',xy=(0.8,0.9),xycoords='axes fraction',size=14)
            
            axr = fig.add_subplot(122)
            krdata = np.loadtxt(s['krdat'])
            
            Ka = np.empty((h.shape[0],3)) # analytically integrated K
            Kn = np.empty((h.shape[0],3)) # numerically integrated K
            
            for i,val in enumerate(h):
                if i%20==0:
                    print i
    
                thetastar = MRC4(val,*props4)
                Ka[i,2] = Kr4(thetastar,val,*props4)*s4['k0']
                Kn[i,2] = MualemNum(fH4,thetastar,val,*props4)*s4['k0']
            
                thetastar = MRC3(val,*(props3))
                Ka[i,1] = Kr3(thetastar,val,*(props3))*s3['k0']
                Kn[i,1] = MualemNum(fH3,thetastar,val,*(props3))*s3['k0']
                
            axr.loglog(krdata[:,0],krdata[:,1],'ro',markerfacecolor='yellow')
            # 4-parameter
            axr.loglog(h,Kn[:,2],'k--') # numeric
            axr.loglog(h,Ka[:,2],'k-')  # analytical
            
            # 3-parameter
            axr.loglog(h,Kn[:,1],'r--')  # numeric
            axr.loglog(h,Ka[:,1],'r-')  # analytical
            
            axr.set_xlabel('$h$ [cm]',fontsize=alfs)
            axr.set_ylabel('$K_r(h)$',fontsize=alfs)
            axr.set_xlim(s['xlim'][1])
            axr.set_ylim(s['ylim'][1])
            axr.annotate('(b)',xy=(0.8,0.9),xycoords='axes fraction',size=14)
            #plt.subplots_adjust(wspace=0.4)
            plt.tight_layout()
            plt.savefig(s['prefix']+'_BOTH_figures.pdf')
            plt.savefig(s['prefix']+'_BOTH_figures.png')
            plt.close(1)
    
    if 1:
        # compare H pdfs for 2,3 and 4-parameter models
        # using Hygiene Sandstone for parameters
        fig = plt.figure(1,figsize=(4,4))
        ax = fig.add_subplot(111)
        h = np.logspace(-1,3)
        fH = np.zeros((h.shape[0],3))
        
        # same parameters used in gnuplot script to make figure
        muz = m.mpf('-6.80')
        sigz = m.mpf('1.0')
        rm = m.mpf('0.1')
        r0 = m.mpf('0.0001')
        
        for i,val in enumerate(h):
            fH[i,0] = fH2(val,sigz,muz)
            fH[i,1] = fH3(val,sigz,muz,rm)
            fH[i,2] = fH4(val,sigz,muz,rm,r0)

        ax.loglog(h,fH[:,2],'r-',linewidth=3,label='proposed 4-par')  # proposed 4-parameter        
        ax.loglog(h,fH[:,1],'g:',linewidth=3,label='Kosugi (1994) 3-par') # Kosugi 3-parameter
        ax.loglog(h,fH[:,0],'k--',linewidth=3,label='classical 2-par')  # classic 2-parameter

        ax.set_ylim([1.0E-4,0.04])
        ax.set_xlim([1.0E0,1.0E3])
        ax.tick_params(axis='both', which='major', labelsize=14)
        ax.set_xlabel('$h$ [cm]',fontsize=18)
        ax.set_ylabel('PDF $f_H(h)$',fontsize=18)
        plt.legend(loc=9,frameon=False,prop={'size':14})
        plt.tight_layout()
        plt.savefig('figure2_pdf_comparison.pdf')
        plt.savefig('figure2_pdf_comparison.png')
        plt.close(1)
    
    if 0:
        for ss in soils:
            fig = plt.figure(1)
            ax = fig.add_subplot(111)
            h = np.logspace(-4,4,150)
            fH = np.zeros((h.shape[0],3))
            
            s = ss['MRC4']
    
            rm = s['rm']
            r0 = s['r0']
            muz = s['muz']
            sigz = s['sigz']
        
            for i,val in enumerate(h):
                fH[i,0] = fH2(val,sigz,muz)/val
                fH[i,1] = fH3(val,sigz,muz,rm)/val
                fH[i,2] = fH4(val,sigz,muz,rm,r0)/val
            
            ax.loglog(h,fH[:,0],'r-')  # classic 2-parameter
            ax.loglog(h,fH[:,1],'k--') # Kosugi 3-parameter
            ax.axvline(kappa/r0)
            ax.axvline(kappa/rm)
            ax.loglog(h,fH[:,2],'g:')  # proposed 4-parameter
            ax.set_ylim([1.0E-7,1.0E-3])
            ax.set_xlim([0,1.0E3])
            ax.set_xlabel('$h$ [cm]',fontsize=alfs)
            ax.set_ylabel('PDF $f_H(h)$',fontsize=alfs)
            plt.tight_layout()
            plt.savefig(s['prefix']+'_integrand_comparison.png')
            plt.close(1)
    
    if optimize:
        
        # optimization using scipy
        kappa = 0.149
        lnkap = np.log(kappa)
        sqrt2 = np.sqrt(2.0)
    
        def optMRC3(x,*args):
    
            sigz,muz,rmax,s0,s1 = x[0:5]
            h,theta = args[0:2]
    
            mueta = lnkap - muz
            hc = kappa/rmax
            srange = s1-s0
            rtsz = 1.4142135623730951*sigz
    
            mrc = np.where(h>hc, 0.5*erfc((np.log(h-hc)-mueta)/rtsz), 1.0)
            mrc *= srange
            mrc += s0
            return np.sum((mrc-theta)**2)
                
        def optMRC4(x,*args):
    
            sigz,muz,rmax,r0,s0,s1 = x[0:6]
            h,theta = args[0:2]
    
            mueta = lnkap - muz
            hc = kappa/rmax
            hmax = kappa/r0
            srange = s1-s0
            rtsz = 1.4142135623730951*sigz
    
            m = np.logical_and(h>hc, h<hmax)
            mrc = np.where(m, 0.5*erfc((np.log(1/(1/h-1/hmax)-hc)-mueta)/rtsz), 1.0)
            mrc[h >= hmax] = 0.0
            mrc *= srange
            mrc += s0        
    
            return np.sum((mrc-theta)**2)
    
        def optBOTH3(x,*args):
    
            sigz,muz,rmax,s0,s1,k0 = x[0:6]
            th,theta,kh,lnkr = args[0:4]
    
            mueta = lnkap - muz
            hc = kappa/rmax
            srange = s1-s0
            msz = mueta-sigz**2
            rtsz = 1.4142135623730951*sigz
    
            mrc = np.where(th>hc, 0.5*erfc((np.log(th-hc)-mueta)/rtsz), 1.0)
            mrc *= srange
            mrc += s0
    
            m = kh>hc
            rtSe = np.sqrt(np.where(m, 0.5*erfc((np.log(kh-hc)-mueta)/rtsz), 1.0))
            perm = np.where(m, rtSe*(0.5*erfc((np.log(kh-hc)-msz)/rtsz))**2, 1.0)
            perm *= k0
    
            # convert residuals to range 0-1 before combining
            return (np.sum(((lnkr-np.log(perm))/np.ptp(lnkr))**2) + 
                    np.sum(((mrc-theta)/np.ptp(theta))**2))
                
        def optBOTH4(x,*args):
    
            sigz,muz,rmax,r0,s0,s1,k0 = x[0:7]
            th,theta,kh,lnkr = args[0:4]
    
            mueta = lnkap - muz
            hc = kappa/rmax
            hmax = kappa/r0
            srange = s1-s0
            msz = mueta-sigz**2
            rtsz = 1.4142135623730951*sigz
    
            m = np.logical_and(th>hc, th<hmax)
            mrc = np.where(m, 0.5*erfc((np.log(1/(1/th-1/hmax)-hc)-mueta)/rtsz), 1.0)
            mrc[th >= hmax] = 0.0
            mrc *= srange
            mrc += s0
    
            m = np.logical_and(kh>hc, kh<hmax)
            u = 1/(1/kh - 1/hmax) - hc
            rtSe = np.sqrt(np.where(m, 0.5*erfc((np.log(u) - mueta)/rtsz), 1.0))
            rtSe[kh >= hmax] = 0.0 
    
            perm = np.where(m, rtSe*(0.5*erfc((np.log(u) - msz)/rtsz))**2, 1.0)
            perm[kh >= hmax] = 1.0E-200 # minimum K
            perm *= k0
    
            # convert residuals to unit interval before combining
            return (np.sum(((lnkr-np.log(perm))/np.ptp(lnkr))**2) + 
                    np.sum(((mrc-theta)/np.ptp(theta))**2))
    
        fh = open('optimization.out','w')
        # optimize against MRC data only
        for ss in soils:
            fh.write('soil: %s\n' % ss['prefix'])
            mrcdata = np.loadtxt(ss['mrcdat'])
    
            fh.write('fit: MRC3\n')
            s = ss['MRC3']
            x0 = np.array([s['sigz'],s['muz'],s['rm'],s['s0'],s['s1']])
            args = (mrcdata[:,0],mrcdata[:,1])
            results = fmin_l_bfgs_b(func=optMRC3, x0=x0, args=args, approx_grad=1)
            fh.write('sigz=%.6g  muz=%.6g  rm=%.6g  s0=%.6g  s1=%.6g\n' % tuple(results[0]))
            print ss['prefix'],'MRC3'
            print results
        
            print 
            fh.write('soil: %s\n' % ss['prefix'])
            fh.write('fit: MRC4\n')
            s = ss['MRC4']
            x0 = np.array([s['sigz'],s['muz'],s['rm'],s['r0'],s['s0'],s['s1']])
            results = fmin_l_bfgs_b(func=optMRC4, x0=x0, args=args, approx_grad=1)
            fh.write('sigz=%.6g  muz=%.6g  rm=%.6g  r0=%.6g  s0=%.6g  s1=%.6g\n' % tuple(results[0]))
            print ss['prefix'],'MRC4'
            print results
            print 
    
        # optimize using K and MRC data
        for ss in soils:
            fh.write('soil: %s\n'%ss['prefix'])
            mrcdata = np.loadtxt(ss['mrcdat'])
            krdata = np.loadtxt(ss['krdat'])
    
            fh.write('fit: BOTH3\n')
            s = ss['BOTH3']
            x0 = np.array([s['sigz'],s['muz'],s['rm'],s['s0'],s['s1'],1.0])
            args = (mrcdata[:,0],mrcdata[:,1],krdata[:,0],np.log(krdata[:,1]))
            results = fmin_l_bfgs_b(func=optBOTH3, x0=x0, args=args, approx_grad=1)
            fh.write('sigz=%.6g  muz=%.6g  rm=%.6g  s0=%.6g  s1=%.6g  k0=%.6g\n' % tuple(results[0]))
            print ss['prefix'],'BOTH3'
            print results
        
            print 
            fh.write('soil: %s\n'%ss['prefix'])
            fh.write('fit: BOTH4\n')
            s = ss['BOTH4']
            x0 = np.array([s['sigz'],s['muz'],s['rm'],s['r0'],s['s0'],s['s1'],1.0])
            results = fmin_l_bfgs_b(func=optBOTH4, x0=x0, args=args, approx_grad=1)
            fh.write('sigz=%.6g  muz=%.6g  rm=%.6g  r0=%.6g  s0=%.6g  s1=%.6g  k0=%.6g\n' % tuple(results[0]))
            print ss['prefix'],'BOTH4'
            print results
            print 
    
        fh.close()
