function t = mrc_th2param_model(p,h)

% returns dimensionless theta*

%thr  = p(1);
%ths  = p(2);
sigz = p(3);
muz  = p(4);

mueta = -1.9038 - muz;  % log(0.0149) = -1.9038
rt2z  = 1.4142135623730951*sigz;

t = zeros(1,length(h));

for i=1:length(h)     
  t(i) = 0.5*erfc((log(h(i))-mueta)/rt2z);
end
