function t = mrc_model(p,h)

kap  = 0.149;
thr  = p(1);
ths  = p(2);
r0   = p(3);
rmax = p(4);
sigz = p(5);
muz  = p(6);

if r0 >= rmax || thr >= ths
  t = -ones(1,length(h));
else
  hmax = kap/r0;
  hc   = kap/rmax;

  mueta = log(kap) - muz;
  rt2z  = sqrt(2.0)*sigz;

  t = zeros(1,length(h));

  for i=1:length(h)
    if h(i) > hmax
      t(i) = thr;
    elseif h(i) < hc
      t(i) = ths;
    else
      u = 1.0/(1.0/h(i) - 1.0/hmax) - hc;
      t(i) = (ths-thr)*0.5*erfc((log(u)-mueta)/rt2z) + thr;
    end
  end
end
