
clear;
load fit_results_3param_dual_2.mat

npar = 6;
colors = ['r','g','b','c','m','k'];
vars = {'\theta_r','\theta_s','log(r_{max})','\sigma_z','\mu_z','K_0'};
Sequences(:,6,:) = 10.0.^Sequences(:,6,:);


if 1
    figure(1);
    for j=1:npar
        subplot(npar,1,j)
        for i=1:npar
            plot(Sequences(:,j,i),[colors(i),'-']);
            hold on;
        end
        ylabel(vars{j})
    end
end

if 1
    samp = 20000;
    figure(2);
    for j=1:npar
        subplot(3,2,j)
        tmp = reshape(Sequences(end-(samp-1):end,j,:),samp*(npar),1);
        hist(tmp,50);
        xlabel(vars{j});
    end
end

if 1    
    % load data
    s = load('mrc_002.dat');
    k = load('kh_002.dat');
    samp = 3000;
    tmp = zeros(npar,samp*npar);
    for i = 1:npar
        tmp(i,:) = reshape(Sequences(end-(samp-1):end,i,:),samp*npar,1);
    end
    
    tmp(3,:) = 10.0.^tmp(3,:);
    
    figure(3);
    subplot(121);
    hv = logspace(log10(s(1,1)*0.5),log10(s(end,1)*5.0));
   
    for j=1:samp
        t = mrc_3param_model(tmp(:,j),hv);
        semilogx(hv,t,'k-','LineWidth',0.1);
        hold on;
    end
    semilogx(s(:,1),s(:,2),'ro','MarkerFaceColor','r');
    xlabel('h');
    ylabel('\theta');
    
    subplot(122);
    hv = logspace(log10(k(1,1)*0.9),log10(k(end,1)*2.0));
    for j=1:samp
        kr = kh_3param_model(tmp(:,j),hv);
        loglog(hv,kr,'k-','LineWidth',0.1);
        hold on;
    end
    loglog(k(:,1),k(:,2),'go','MarkerFaceColor','g');
    xlabel('h');
    ylabel('k_r');
        
end
        
