% ----------------- DiffeRential Evolution Adaptive Metropolis algorithm -----------------------%
%                                                                                               %
% DREAM runs multiple different chains simultaneously for global exploration, and automatically %
% tunes the scale and orientation of the proposal distribution using differential evolution.    %
% The algorithm maintains detailed balance and ergodicity and works well and efficient for a    %
% large range of problems, especially in the presence of high-dimensionality and                %
% multimodality. 							                                                    %                     
%                                                                                               %
% DREAM developed by Jasper A. Vrugt and Cajo ter Braak                                         %
%                                                                                               %
% This algorithm has been described in:                                                         %
%                                                                                               %
%   Vrugt, J.A., C.J.F. ter Braak, M.P. Clark, J.M. Hyman, and B.A. Robinson, Treatment of      %
%      input uncertainty in hydrologic modeling: Doing hydrology backward with Markov chain     %
%      Monte Carlo simulation, Water Resources Research, 44, W00B09, doi:10.1029/2007WR006720,  %
%      2008.                                                                                    %
%                                                                                               %
%   Vrugt, J.A., C.J.F. ter Braak, C.G.H. Diks, D. Higdon, B.A. Robinson, and J.M. Hyman,       %
%       Accelerating Markov chain Monte Carlo simulation by differential evolution with         %
%       self-adaptive randomized subspace sampling, International Journal of Nonlinear          %
%       Sciences and Numerical Simulation, 10(3), 271-288, 2009.                                %
%                                                                                               %
%   Vrugt, J.A., C.J.F. ter Braak, H.V. Gupta, and B.A. Robinson, Equifinality of formal        %
%       (DREAM) and informal (GLUE) Bayesian approaches in hydrologic modeling?, Stochastic     %
%       Environmental Research and Risk Assessment, 1-16, doi:10.1007/s00477-008-0274-y, 2009,  %
%       In Press.                                                                               %   
%                                                                                               %
% For more information please read:                                                             %
%                                                                                               %
%   Ter Braak, C.J.F., A Markov Chain Monte Carlo version of the genetic algorithm Differential %
%       Evolution: easy Bayesian computing for real parameter spaces, Stat. Comput., 16,        %
%       239 - 249, doi:10.1007/s11222-006-8769-1, 2006.                                         %
%                                                                                               %
%   Vrugt, J.A., H.V. Gupta, W. Bouten and S. Sorooshian, A Shuffled Complex Evolution          %
%       Metropolis algorithm for optimization and uncertainty assessment of hydrologic model    %
%       parameters, Water Resour. Res., 39 (8), 1201, doi:10.1029/2002WR001642, 2003.           %
%                                                                                               %
%   Ter Braak, C.J.F., and J.A. Vrugt, Differential Evolution Markov Chain with snooker updater %
%       and fewer chains, Statistics and Computing, 10.1007/s11222-008-9104-9, 2008.            %
%                                                                                               %
%   Vrugt, J.A., C.J.F. ter Braak, and J.M. Hyman, Differential evolution adaptive Metropolis   %
%       with snooker update and sampling from past states, SIAM journal on Optimization, 2009.  %
%                                                                                               %
%   Vrugt, J.A., C.J.F. ter Braak, and J.M. Hyman, Parallel Markov chain Monte Carlo simulation %
%       on distributed computing networks using multi-try Metropolis with sampling from past    %
%       states, SIAM journal on Scientific Computing, 2009.                                     %
%                                                                                               %
% Copyright (c) 2008, Los Alamos National Security, LLC                                         %
% All rights reserved.                                                                          %
%                                                                                               %
% Copyright 2008. Los Alamos National Security, LLC. This software was produced under U.S.      %
% Government contract DE-AC52-06NA25396 for Los Alamos National Laboratory (LANL), which is     %
% operated by Los Alamos National Security, LLC for the U.S. Department of Energy. The U.S.     %
% Government has rights to use, reproduce, and distribute this software.                        %
%                                                                                               %
% NEITHER THE GOVERNMENT NOR LOS ALAMOS NATIONAL SECURITY, LLC MAKES A NY WARRANTY, EXPRESS OR  %
% IMPLIED, OR ASSUMES ANY LIABILITY FOR THE USE OF THIS SOFTWARE.  If software is modified to   %
% produce derivative works, such modified software should be clearly marked, so as not to       %
% confuse it with the version available from LANL.                                              %
%                                                                                               %
% Additionally, redistribution and use in source and binary forms, with or without              %
% modification, are permitted provided that the following conditions are met:                   %
% � Redistributions of source code must retain the above copyright notice, this list of         %
%   conditions and the following disclaimer.                                                    %
% � Redistributions in binary form must reproduce the above copyright notice, this list of      %
%   conditions and the following disclaimer in the documentation and/or other materials         %
%   provided with the distribution.                                                             %
% � Neither the name of Los Alamos National Security, LLC, Los Alamos National Laboratory, LANL %
%   the U.S. Government, nor the names of its contributors may be used to endorse or promote    %
%   products derived from this software without specific prior written permission.              %
%                                                                                               %
% THIS SOFTWARE IS PROVIDED BY LOS ALAMOS NATIONAL SECURITY, LLC AND CONTRIBUTORS "AS IS" AND   %
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES      %
% OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL LOS %
% ALAMOS NATIONAL SECURITY, LLC OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, %
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF   %
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)        %
% HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT %
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,       %
% EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                                            %
%                                                                                               %
% MATLAB code written by Jasper A. Vrugt, Center for NonLinear Studies (CNLS)                   %
%                                                                                               %
% Written by Jasper A. Vrugt: vrugt@lanl.gov                                                    %
%                                                                                               %
% Version 0.5: June 2008                                                                        %
% Version 1.0: October 2008         Adaption updated and generalized CR implementation          %
%                                                                                               %
% --------------------------------------------------------------------------------------------- %

% ----------------------------------------------------------------------------------------------%
%                                                                                               %
%           Note: DE-MC of ter Braak et al. (2006) is a special variant of DREAM                %
%                     It can be executed using the following options                            %
%                                                                                               %
%       MCMCPar.seq = 2 * MCMCPar.n;                                                            %
%       MCMCPar.DEpairs = 1;                                                                    %
%       MCMCPar.nCR = 1;                                                                        %
%       MCMCPar.eps = 0;                                                                        %
%       MCMCPar.outlierTest = 'None';                                                           % 
%       Extra.pCR = 'Fixed'                                                                     %
%                                                                                               %
% ----------------------------------------------------------------------------------------------%

% ----------------------------------------------------------------------------------------------%
%                                                                                               %
%   After termination of DREAM you can generate a 2D matrix with samples using the command:     %
%                                                                                               %
%       ParSet = genparset(Sequences,MCMCPar); if Extra.save_in_memory = 'Yes'                  %
%       otherwise                                                                               %
%       ParSet = GenParSet(Reduced_Seq,MCMCPar); if Extra.reduced_sample_collection = 'Yes'     %
%                                                                                               %
%       output.R_stat   The Gelman Rubin convergence diagnostic                                 %
%       output.AR       The acceptance percentage of candidate points                           %
%       output.CR       Optimized probabilities for individual crossover rates                  %
%       output.outlier  Outlier chain numbers                                                   %
%       hist_logp       Log density of last 50% of samples in each chain                        %
%       X               Final position of chains (population)                                   %
%                                                                                               %
% ----------------------------------------------------------------------------------------------%

MCMCPar.n = 4;                          % Dimension of the problem
MCMCPar.seq = MCMCPar.n;                % Number of Markov Chains / sequences
MCMCPar.ndraw = 2000000;                   % Maximum number of function evaluations
MCMCPar.nCR = 2;                        % Crossover values used to generate proposals (geometric series)
MCMCPar.Gamma = 0;                      % Kurtosis parameter Bayesian Inference Scheme
MCMCPar.DEpairs = 1;                    % Number of DEpairs
MCMCPar.steps = 10;                     % Number of steps in sem
MCMCPar.eps = 2e-1;                     % Random error for ergodicity
MCMCPar.outlierTest = 'IQR_test';       % What kind of test to detect outlier chains?

% -----------------------------------------------------------------------------------------------------------------------
Extra.pCR = 'Update';                   % Adaptive tuning of crossover values
% -----------------------------------------------------------------------------------------------------------------------

% --------------------------------------- Added for reduced sample storage ----------------------------------------------
Extra.reduced_sample_collection = 'No'; % Thinned sample collection?
Extra.T = 1;                         % Every Tth sample is collected
% -----------------------------------------------------------------------------------------------------------------------

% What type of initial sampling
Extra.InitPopulation = 'LHS_BASED';

% all three soils have the same starting values
% Give the parameter ranges (minimum and maximum values)
%                thr    ths   sig    mu
ParRange.minn = [0.00, 0.00, 0.0, -20.0]; 
ParRange.maxn = [1.00, 1.00, 20.0,   0.0];
Extra.save_in_memory = 'Yes';
Extra.BoundHandling = 'Reflect';

global soil;
global idx;

for soil=1:3
  idx = 0;

  fn = ['mrc_00',num2str(soil),'.dat'];

  clear mrc Measurement.MeasData Extra.h;

  % data are h and theta
  mrc = load(fn);

  Measurement.MeasData = mrc(:,2);
  Measurement.Sigma = [];
  Measurement.Weight = ones(size(mrc,1),1);
  Measurement.N = size(Measurement.MeasData,1);

  Extra.ht = mrc(:,1);

  fn = ['kh_00',num2str(soil),'.dat'];
  kr = load(fn);
  Extra.kr = kr(:,:);

  ModelName = 'fit_soils';

%%  disp(fn);
  more off;
  format short;
  format compact;

  % Define likelihood function -- Sum of Squared Error
  option = 3; 
  Measurement.Sigma = ones(Measurement.N,1);

  % Scale of Delayed Rejection if used in algorithm
  Extra.DR = 'No'; 
  Extra.DRscale = 10;

  clear Sequences Reduced_Seq X output hist_logp;

  % Run the MCMC algorithm
  [Sequences,Reduced_Seq,X,output,hist_logp] = ...
      dream(MCMCPar,ParRange,Measurement,ModelName,Extra,option);

  save(['fit_results_2param_mrc_only',num2str(soil),'.mat'],'-v7');
end
