import pickle
from scipy import *

data = pickle.load(open('mrc_kh_pickle','r'))

# define traditional van Genuchten model
def vgt(p, *args):
    thr = p[0]; ths = p[1]
    alpha = p[2]; n = p[3]
    h = array(args[0])  # vector of observed water content
    t = array(args[1])  # corresponding vector of pressure head
    
    # estimated theta
    th = (thr-ths)*(1.0 + (alpha*h)**n)**(1.0/n - 1.0) + thr
    return sum((th - t)**2)

res = {}

# loop over soils 
for id in data:
    # initial guess from avarage parameter of Schaap 2006
    p0 = [0.055,0.442,0.0219,1.64]
    res[id] = optimize.fmin(vgt,p0,args=[data[id]['mrc'][0],data[id]['mrc'][1]])

pickle.dump(res, open('vgt_fit_results_pickle','w'))
