function k = kh_4param_model(p,h)

  kap  = 0.149;
  %thr  = p(1);
  %ths  = p(2);
  r0   = p(3);
  rmax = p(4);
  sigz = p(5);
  muz  = p(6);
  K0   = p(7);

  Se = mrc_th4param_model(p(1:6),h);

  hmax = kap/r0;
  hc   = kap/rmax;
  memsz2 = -1.9038 - muz - sigz^2;

  mink = 1.0E-100;
  rt2z = 1.4142135623730951*sigz;

  k = zeros(1,length(h));

  for i=1:length(h)
    if h(i) <= hc
      k(i) = K0;
    elseif h(i) >= hmax
      k(i) = mink;
    else
      k(i) = max( mink, K0*sqrt(Se(i))* ...
                 (0.5*erfc((log(1.0/(1.0/h(i) - 1.0/hmax) - hc) - memsz2)/rt2z))^2 );
    end
  end
