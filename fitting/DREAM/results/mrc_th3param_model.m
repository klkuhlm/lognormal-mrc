function t = mrc_th3param_model(p,h)

% return dimenesionless theta*

kap  = 0.149;
%thr  = p(1);
%ths  = p(2);
rmax = p(3);
sigz = p(4);
muz  = p(5);

hc   = kap/rmax;

mueta = -1.9038 - muz;  % log(0.0149) = -1.9038
rt2z  = 1.4142135623730951*sigz;

t = zeros(1,length(h));

for i=1:length(h)
  if h(i) <= hc
    t(i) = 1.0;
  else
    t(i) = 0.5*erfc((log(h(i) - hc)-mueta)/rt2z);
  end
end
