function t = mrc_3param_model(p,h)

kap  = 0.149;
thr  = p(1);
ths  = p(2);
rmax = p(3);
sigz = p(4);
muz  = p(5);

global soil;
global idx;

if mod(idx,100) == 0
  printf("%d %d %.4e %.4e %.4e %.4e %.4e\n",soil,idx,thr,ths,rmax,sigz,muz);
end

idx = idx + 1;

if thr >= ths
  t = -ones(1,length(h))*9999999999.9;
else
  hc   = kap/rmax;

  mueta = log(kap) - muz;
  rt2z  = 1.4142135623730951*sigz;

  t = zeros(1,length(h));

  for i=1:length(h)
    if h(i) <= hc
      t(i) = ths;
    else
      u = h(i) - hc;
      t(i) = (ths-thr)*0.5*erfc((log(u)-mueta)/rt2z) + thr;
    end
  end
end
