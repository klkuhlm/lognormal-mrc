# $Id: kh_models.py,v 1.5 2008/06/17 23:38:18 kris Exp kris $
import mrc_models

# traditional van Genuchten model
def mvgt(p,h):
    thr = p[0]; ths = p[1]
    n = p[3]  # alpha not used
    K0 = p[4]; L = p[5]

    # compute saturation 
    Se = (mrc_models.vgt(p[0:4],h) - thr)/(ths - thr)

    m = 1.0 - 1.0/n

    return K0*Se**L*(1.0 - (1.0 - Se**(1.0/m))**m)**2
    

# four-parameter lognormal model
def fourplnk(p,h):
    from numpy import zeros,array,log,concatenate
    from scipy.special import erfc
    from math import sqrt

    kap = 0.149 # cm^2
    thr = p[0]; ths = p[1]
    r0 = p[2];   rmax = p[3]
    sigz = p[4]; muz = p[5]
    K0 = p[6]; L = p[7]

    tiny = 1.0E-100

    # only way to ensure proper parameter bounds?
    if r0>=rmax or r0<1.1E-20 or rmax>1.2E+20 or K0<1.0E-7 or K0>1.0E+7 or L<-10.0 or L>10.0:
        return zeros((len(h),)) + 1.0E-50
    else:
        Se = (mrc_models.fourpln(p[0:6],array(h)) - thr)/(ths - thr)
        
        hmax = kap/r0
        hc = kap/rmax
        rt2z = sqrt(2.0)*sigz
        mueta = log(kap) - muz
                
        klow = [K0 for hval in h if hval < hc]
        khi = [tiny for hval in h if hval > hmax]
        
        # assuming h is sorted to be monotonically increasing
        # use lengths of klow and khigh to compute which portion of k(h) can
        # be computed a vectorized fashion 

        lo = len(klow)
        hi = len(h) - len(khi) 

        kmid = K0*Se[lo:hi]**L*(0.5* erfc((log(1.0/(1.0/h[lo:hi] - 1.0/hmax) - hc) - \
                                               (mueta - sigz**2))/rt2z))**2

        return concatenate([klow,kmid,khi])
