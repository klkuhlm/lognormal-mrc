# $Id: load_data.py,v 1.4 2008/05/23 00:05:29 kris Exp kris $

import pickle

# slurp files into lists of lines (headers and footers already removed)
ssclines = open('SSCBDKs_235.txt','r').readlines()
kdatlines = open('K_h_row_235.txt','r').readlines()
mrclines = open('Theta_h_2134.txt','r').readlines()

# database of data is a dictionary
data = {}

# first column of ssc datafile will be dictionary key
for line in ssclines:
    key = line.split()[0]
    # each record is a dictionary

    # save USDA textures as an integer
    texsub = line.split()[8:20].index('1')
    tex = line.split()[20:24].index('1')
        
    rec = {'id':key,
           'ssc': tuple(float(x) for x in line.split()[2:5]),
           'bd':  float(line.split()[5]),
           'ks1': float(line.split()[6]),
           'ks2': float(line.split()[7]),
           'USDAsub': texsub, 'USDA': tex}
    data[key] = rec

for line in kdatlines:
    # data read as h,k pairs
    rowdat = line.split()[4:]
    h = tuple(float(x) for x in rowdat[0:-1:2])
    k = tuple(float(x) for x in rowdat[1::2])
    data[line.split()[1]]['kh'] = {'h':h,'k':k}

for line in mrclines:
    # only operate on the 235 soils in other 2 files
    if line.split()[0] in data:
        # data read as theta,h pairs
        rowdat = line.split()[2:]
        h = tuple(float(x) for x in rowdat[0:-1:2])
        t = tuple(float(x) for x in rowdat[1::2])
        data[line.split()[0]]['mrc'] = {'h':h,'t':t}
        
# save the data structure via pickle to be read in by calculation routine
pickle.dump(data, open('mrc_kh_pickle','w'))

print data['142']
