function [] = plot_results(soil,mrc_only,Sequences,Extra,Measurement)

% back transform log-transformed parameters
Sequences(:,3:4,:) = 10.0.^Sequences(:,3:4,:);

if mrc_only
  if soil == 1
    prefix = 'hygeine-mrc-only';
    
  elseif soil == 2		
    prefix = 'siltloamge3-mrc-only';
  else
    prefix = 'beitnetofa-mrc-only';
  end
else
  if soil == 1
    prefix = 'hygeine-dual';
  elseif soil == 2
    prefix = 'siltloamge3-dual';
  else
    prefix = 'beitnetofa-dual';
  end
end

nchains = size(Sequences,3);
nvars = size(Sequences,2) - 2;
nsamp = size(Sequences,1);

%            r       g       b       c       m       k
colors = {[1,0,0],[0,1,0],[0,0,1],[0,1,1],[1,0,1],[0,0,0]};
vars = {'\theta_r','\theta_s','r_0','r_{max}','\sigma_Z','\mu_Z'};

if 1
    % plot figure showing total progress of individual chains
    % ###########################################################

    figure();
    h = {};
    for i=1:nvars
        h{i} = subplot(nvars,1,i);
        for j=1:nchains
            plot(1:nsamp,Sequences(:,i,j),'Color',colors{j});
            hold on;
        end
        axis('tight');
        ylabel(vars{i});
    end
    subplot(nvars,1,nvars);
    xlabel('total iterations');
    
    for i=1:nvars
        subplot(nvars,1,i);
        if (i < nvars)
            % don't remove tickmark labels on bottom plot
            set(gca,'XTickLabel',[]);
        end
        p = get(h{i},'pos'); % left,bottom,width,height (as % of window)
        p(4) = p(4) + 0.02; % increase height of figure by 5% of total
        set(h{i},'pos',p);
    end
        
    set(gcf,'PaperType','usletter');
    orient landscape;
    print('-depsc2',[prefix,'-show-all-chains.eps']);
end


cutoff = floor(nsamp - 50000);
nel = (nsamp - cutoff)*nchains;
nbins = 100;
rSequences = ones(nel,nvars);
for i=1:nvars
    rSequences(:,i) = reshape(Sequences(cutoff+1:end,i,:),[],1);
end

if 1
    % plot figure showing histograms for a portion of data
    % ######################################################

    figure();
    for i=1:nvars
        subplot(3,2,i);
        % normalized histogram from 
        % http://www.mathworks.com/matlabcentral/fileexchange/
        % 22802-normalized-histogram
        [n,xout] = histnorm(rSequences(:,i),nbins);
        [~,idx] = max(n);
        mode = xout(idx);
        m = mean(rSequences(:,i));
        v = var(rSequences(:,i));
        hold on;
        bar(xout,n,'BarWidth',1.0);
        % plot gaussian with same mean & variance in red
        % normal
        plot(xout,exp(-(xout - m).^2./(2*v))./sqrt(2*pi*v),'r-');
        xlabel(vars{i},'FontSize',12);
        ylabel('posterior density','FontSize',9);
        fmtstr = sprintf('%.4g',m);
        text(0.1,0.9,['\mu=',fmtstr],'Units','normalized');
        fmtstr = sprintf('%.4g',mode);
        text(0.1,0.8,['mode=',fmtstr],'Units','normalized');
        fmtstr = sprintf('%.4g',v);
        text(0.7,0.9,['\sigma^{2}=',fmtstr],'Units','normalized');
    end
    
    set(gcf,'PaperType','usletter'); 
    orient landscape;
    print('-depsc2',[prefix,'-all-param-hist.eps']);
    
    figure();
    [~,AX,~,~] = plotmatrix(rSequences);
    for i = 1:nvars
        for j = 1:nvars
            if j==1
                set(get(AX(i,j),'YLabel'),'String',vars{i},'FontSize',9); 
            end
            if i==nvars
                set(get(AX(i,j),'XLabel'),'String',vars{j},'FontSize',9);
            end
        end
    end

    set(gcf,'PaperType','usletter');
    orient landscape;
    print('-depsc2',[prefix,'-all-param-plotmatrix.eps']);
end


if 1
    % plot horsetail of last few runs (after convergence)
    % ##################################################
    last = 5000;
    figure();

    subplot(121);
    ht = logspace(log10(min(Extra.ht))-0.5, ...
                  log10(max(Extra.ht))+0.5);
    for i=nel-last:nel
        p = rSequences(i,:);
        modt = mrc_model(p,ht);
        semilogx(ht,modt,'k-');
	hold on;
    end
    xlabel('h (cm)');
    ylabel('\theta(h)');

    if ~mrc_only
        numhk = size(Extra.hk,1);
        semilogx(Extra.ht,Measurement.MeasData(numhk+1:end),'ro');
    else
        numhk = size(Extra.kr,1);
        semilogx(Extra.ht,Measurement.MeasData(:),'ro');
    end   

    subplot(122);
    if ~mrc_only
        hk = logspace(log10(min(Extra.hk))-0.5, ...
                      log10(max(Extra.hk))+0.5);
    else
        hk = logspace(log10(min(Extra.kr(:,1)))-0.5, ...
                      log10(max(Extra.kr(:,1)))+0.5);
    end
    for i=nel-last:nel
        p = [rSequences(i,:),1.0,0.5];
        modk = kh_model(p,hk);
        loglog(hk,modk,'k-');
        hold on;
    end
    xlabel('h (cm)');
    ylabel('K_r(h)');
    ylim([1.0E-5,10.0]); % some curves go to ~zero
        
    if ~mrc_only
        loglog(Extra.hk,10.0.^Measurement.MeasData(1:numhk),'ro');
    else
        loglog(Extra.kr(:,1),Extra.kr(:,2),'ro');
    end

    set(gcf,'PaperType','usletter');
    orient landscape;
    print('-depsc2',[prefix,'-horsetail.eps']);

end	    
