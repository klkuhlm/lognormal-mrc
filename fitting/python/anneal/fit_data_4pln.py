# $Id: fit_data_4pln.py,v 1.4 2008/06/17 23:50:27 kris Exp kris $

import pickle
import mrc_models 
from numpy import sum, array, logspace, zeros
from scipy.optimize import anneal,fmin_l_bfgs_b

data = pickle.load(open('mrc_kh_pickle','r'))

# wrapper for calling mrc routine (assume r0 and rmax)
def fpln_ls_nor(p, *args):
    h = array(args[0]['h'])  # vector of observed water content
    t = array(args[0]['t'])  # corresponding vector of pressure head

    pin = zeros((6,))

    pin[0] = p[0]*p[1]  # thrmult*ths = thr
    pin[1] = p[1]  # ths

    pin[2] = 1.0E-20  # r0
    pin[3] = 1.0E+20 # rmax

    pin[4:6] = p[2:4]  # sigz and muz

    return sum((mrc_models.fourpln(pin,h) - t)**2)

# wrapper for calling mrc routine 
def fpln_ls(p, *args):
    h = array(args[0]['h'])  # vector of observed water content
    t = array(args[0]['t'])  # corresponding vector of pressure head

    pout = p
    pout[0] = p[0]*p[1]

    return sum(( mrc_models.fourpln(p,h) - t)**2)


# results
results = {}

rmin = logspace(-20,2,num=50)
rmax = logspace(-6,20,num=50)

# loop over soils 
for id in data:
    
    # first parameter is multiplier, not really thr
    maxp = [0.8000,  0.600,  7.0,   1.0]     

    # sand
    if data[id]['USDA'] == 0:
        p0 =   [0.1928,  0.382,  2.0,  -3.0]
        minp = [1.0E-4,  0.133,  0.0, -20.0]

    # loam
    elif data[id]['USDA'] == 1:
        p0 =   [0.1704,  0.475,  2.0,  -3.0]
        minp = [1.0E-4,  0.191,  0.0, -20.0]

    # silt
    elif data[id]['USDA'] == 2:
        p0 =   [0.1966,  0.386,  2.0,  -3.0]
        minp = [1.0E-4,  0.139,  0.0, -20.0]

    # clay
    else:
        p0 =   [0.1764,  0.428,  2.0,  -3.0]
        minp = [1.0E-4,  0.180,  0.0, -20.0]
      

    # use simulated annealing to get into the right ballpark?
    xa,da = anneal(fpln_ls_nor, p0, lower=minp, schedule='fast',
                   upper=maxp,args=[data[id]['mrc']])

    # annealing doesn't return the function associated with the returned solution (WTF?)
    args=[data[id]['mrc']]
    fa = fpln_ls_nor(xa,*args)

    threshold = 0.01
    # if fit is _bad_, try again with different scheduler
    if fa > threshold:
        print 'fa=',fa,' try cauchy'
        xab,dab = anneal(fpln_ls_nor, xa,
                         lower=minp, upper=maxp,
                         schedule='cauchy', args=[data[id]['mrc']])
        fab = fpln_ls_nor(xab,*args)

        if fab < fa and fab < threshold:
            xa,da = xab,dab
            fa = fab
        else:
            print 'fab=',fab,' try boltzmann'
            xab,dab = anneal(fpln_ls_nor, xab, lower=minp, 
                             upper=maxp, schedule='boltzmann', 
                             args=[data[id]['mrc']])
            fab = fpln_ls_nor(xab,*args)
            
            if fab < fa:
                xa,da = xab,dab   
                fa = fab
                        
    # improve estimate using search-based optimizer
    x1,f1,d1 = fmin_l_bfgs_b(fpln_ls_nor,xa,approx_grad=1,
                             bounds=zip(minp,maxp),args=[data[id]['mrc']])


    # increase r0 and decrease rmax until error increases past tolerance
    # these will be used as limits in computing r0 and rmax in K(h)
    # so that they do not ruin the mrc fit at the expense of the K(h) fit


    rminbest = 1.0E-20
    rmaxbest = 1.0E+20
    reltol = 1.01
    args = [data[id]['mrc']]

    p = x1.tolist()
    p.insert(2,1.0E-20)
    p.insert(3,1.0E+20)

    for i in range(len(rmin)):
        p[2] = rmin[i]
        fmin = fpln_ls(p,*args)

        # did this raise the error past tolerance?
        if fmin/f1 > reltol:
            if i > 0:
                rminbest = rmin[i-1]
            break

        # did it make it to end of rmin vector without increasing past tolerance?
        if i == len(rmin)-1:
            rminbest = rmin[len(rmin)-1]        

    p[2] = 1.0E-20
    for i in range(len(rmax)-1,0,-1):  # count backwards
        p[3] = rmax[i]  
        fmax = fpln_ls(p,*args)

        if fmax/f1 > reltol:
            if i < len(rmax)-1:
                rmaxbest = rmax[i+1]
            break

        if i == 0:
            rmaxbest = rmax[0]

    print id,fa,f1,x1,rminbest,rmaxbest
    results[id] = {'xa':xa,'fa':fa,'x1':x1,'f1':f1,'d1':d1,
                   'r':[rminbest,rmaxbest]}


pickle.dump(results, open('fpln_fit_results_pickle','w'))
