This repository has the latex source files and figures associated with
the manuscript published as:

Malama, B. and K.L. Kuhlman, 2015. "Unsaturated Hydraulic Conductivity
Models Based on Truncated Lognormal Pore-size Distributions",
Groundwater, 53(3):498-502.

http://dx.doi.org/10.1111/gwat.12220

A draft of the document can also be freely downloaded from the arXiv:
http://arxiv.org/abs/1301.0772

The "fitting/python/mpmath" directory includes the latest version of
the python scripts used to fit the proposed model to a few datasets. 
The numerical integration of the Mualem (1976) approach is also done there.

The other directories under "fitting" include other attempts and
approaches to do the same thing with other software, or a MCMC
approach to describe the uncertainty associated with fitting several
different models to data.

This model is now implemented in PFLOTRAN as the "modified Kosugi" model.

https://bitbucket.org/pflotran/pflotran/wiki/Home

http://www.documentation.pflotran.org/user_guide/cards/process_model_cards/characteristic_curves_card.html

http://www.documentation.pflotran.org/qa_tests/pc_sat_rel_perm.html
