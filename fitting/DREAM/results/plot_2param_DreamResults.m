
clear;
load fit_results_2param_dual_1.mat

npar = 5;
colors = ['r','g','b','m','k'];
vars = {'\theta_r','\theta_s','\sigma_z','\mu_z','K_0'};
Sequences(:,5,:) = 10.0.^Sequences(:,5,:);

shift = 0; % set to 1 for soil number 1, which has a bad chain (first one)

if 1
    figure(1);
    for j=1:npar
        subplot(npar,1,j)
        for i=(1+shift):npar
            plot(Sequences(:,j,i),[colors(i),'-']);
            hold on;
        end
        ylabel(vars{j})
    end
end

if 0
    samp = 20000;
    figure(2);
    for j=1:npar
        subplot(3,2,j)
        tmp = reshape(Sequences(end-(samp-1):end,j,(1+shift):end),samp*(npar-shift),1);
        hist(tmp,50);
        xlabel(vars{j});
    end
end

if 0    
     % load data
    s = load('mrc_001.dat');
    k = load('kh_001.dat');
    samp = 3000;
    tmp = zeros(npar,samp*(npar-shift));
    for i = 1:npar
        tmp(i,:) = reshape(Sequences(end-(samp-1):end,i,(1+shift):end),samp*(npar-shift),1);
    end
    
    figure(3);
    subplot(121);
    hv = logspace(log10(s(1,1)*0.5),log10(s(end,1)*5.0));
   
    for j=1:samp
        t = mrc_2param_model(tmp(:,j),hv);
        semilogx(hv,t,'k-','LineWidth',0.1);
        hold on;
    end
    semilogx(s(:,1),s(:,2),'ro','MarkerFaceColor','r');
    xlabel('h');
    ylabel('\theta');
    
    subplot(122);
    hv = logspace(log10(k(1,1)*0.9),log10(k(end,1)*2.0));
    for j=1:samp
        kr = kh_2param_model(tmp(:,j),hv);
        loglog(hv,kr,'k-','LineWidth',0.1);
        hold on;
    end
    loglog(k(:,1),k(:,2),'go','MarkerFaceColor','g');
    xlabel('h');
    ylabel('k_r');
        
        
end
        
