function k = kh_2param_model(p,h)

  %%kap  = 0.149;
  %thr  = p(1);
  %ths  = p(2);
  sigz = p(3);
  muz  = p(4);
  K0   = p(5);

  Se = mrc_th2param_model(p(1:4),h);

  memsz2 = (-1.9038 - muz) - sigz^2;

  mink = 1.0E-100;
  rt2sz = 1.4142135623730951*sigz;

  k = zeros(1,length(h));
    
  for i=1:length(h)
    k(i) = max( mink, K0*sqrt(Se(i))* ...
               (0.5*erfc((log(h(i)) - memsz2)/rt2sz))^2 );
  end
 
