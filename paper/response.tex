\documentclass[11pt,letterpaper]{article}
\usepackage{fullpage,color}

\title{Response to reviewer's comments on ``Models for Unsaturate Hydraulic Conductivity Based on Truncated Lognormal Pore-size Distributions'' (\texttt{GW20130926-0206})}

\author{Bwalya Malama and Kristopher L. Kuhlman}

\begin{document}

\maketitle

This document includes detailed responses to the comments raised by reviewers on our manuscript ``Models for Unsaturate Hydraulic Conductivity Based on Truncated Lognormal Pore-size Distributions'' submitted to \textit{Groundwater} (\texttt{GW20130926-0206}).  We include a version of the revised manuscript with changes tracked, as well as a final ``clean'' version of the manuscript.  Line numbers referring to the revised manuscript are associated with the ``marked up'' copy with comments.  We intend the manuscript to be considered a Methods Note submission.  

The manuscript focuses on the use of an alternative approach to Mualem's method for deriving  closed-form hydraulic conductivity functions from  known moisture retention functions.  Using this approach we derive an approximate closed-form solution for Kosugi's well-known 3-parameter lognormal model, and develop an analogous 4-parameter form of Kosugi's model. We have attempted to respond adequately to reviewers' comments for more material and discussion, while maintaining brevity required by a Methods Note.  

The manuscript was written using \LaTeX~and included vector encapsulated postscript illustrations, and was internally reviewed at Sandia. The reviewed manuscript was then converted to MS-Word for submission to Groundwater. We  apologize for errors and poor-quality figures introduced into the manuscript during the conversion process (we thank the reviewers for catching two typos in the equations). We feel the journal Groundwater would be well-served by modifying its process and accepting \LaTeX~submissions directly (like Elsevier and Springer journals).

Reviewers' comments are presented in smaller italic font, our responses are below in normal font.  Some of the reviewers' multiple-part questions were broken up to facilitate direct responses. 

\section{Editor's Comments}
\textit{\footnotesize There are several major themes that run through the reviewer's comments.}

\begin{itemize}
\item \textit{\footnotesize First, several reviewers are interested in seeing a broader context provided by a better review of existing models (Reviewer 2) and a comparison of your model with others with other data (Reviewer 1).}

We have added additional motivation and background material at the beginning of the introduction (lines 33--51 of revised manuscript). We want to keep the manuscript short so it can maintain its status as a Methods Note.  We don't feel further example soils would provide any more insight into the strategy we used to develop a closed-form hydraulic conductivity model for a three- and four-parameter lognormal moisture retention curve model.  

\item \textit{\footnotesize Second, two reviewers would like to see the manuscript published as a Methods Note. Your paper is about the correct length now, so any additions will need to be judicious in the use of space or make use of "Supporting Information".}

We still intend the  manuscript to be a Methods Note; we have sought to maintain brevity while accommodating revisions and comments.

\item \textit{\footnotesize Finally, the AE (Reviewer 3) is concerned about inconsistent units. So, please recheck your derivations carefully.}

We have fixed the typo in Equation 2 that led to the dimensional inconsistency there.  We do not agree with the reviewer's apparent further contention that logarithm arguments must also be dimensionless.  
\end{itemize}

\begin{enumerate}
\item \textit{\footnotesize  Our layout does not use heading numbering. Please use another arrangement for headings.}

Numbering was removed from headings.

\item \textit{\footnotesize Figures 1 and 2 look rough. Our system tends to cause pixilation. But, these look like they have other problems. I typically import to Adobe Illustrator to get vector symbols that are always sharper.}

Vector illustrations are provided for all figures.

\item \textit{\footnotesize Please layout the figures to use minimal space. For example Figure 1 could have (a) and (b) on top of each other.}

The two parts of Figure 1 have been combined into a single figure.  All figures have had their font sizes increase and been modified to optimize layout in journal columns.  

\end{enumerate}

\section{Reviewer 1}
\textit{\footnotesize This paper introduced a transformation to Mualem formulation of relative permeability to allow the integration of relative permeability model. This procedure resulted in closed form analytical solutions of relative permeability while retaining the original parameters of the Brutsaret and Kosugi. Kosugi earlier work assumed a truncated lognormal pore size distribution to derive water retention model curve.}

\begin{itemize}
\item \textit{\footnotesize Overall this new paper is well written, but I am not sure how this new mathematistry will contribute to better representation of soil moisture characteristics. I will support a new paper with more \underline{extensive comparison} of the different models applied to an \underline{extensive database of data} on permeability and water retention.}

As the reviewer indicates (underline emphasis added), this would be an extensive effort -- far beyond the scope of a Methods Note.  This manuscript was focused on the development of an approach to extend closed-form expressions to models which did not have them previously.

\item \textit{\footnotesize It is already established that Mualem model of relative permeability won't provide a good representation without some actual permeability data. It remains unknown whether their modification to Mualem model to secure the closed form integration is doing any better job.}

We do not propose the proposed approximate scheme is better than the original Mualem approach, it simply extends its applicability to more cases.  Closed-form expressions for moisture retention curves and unsaturated hydraulic conductivity models are useful in several ways:
\begin{itemize}
\item straightforward to implement in programs;
\item avoid numerical integration errors;
\item avoid computational burden of numerical integration; and
\item provide more insight into relationships between parameters.
\end{itemize}
The importance of closed-form expressions for these models is now discussed in the beginning of the introduction (lines 33--51 of the revised manuscript).

\item \textit{\footnotesize Without this additional work, the article might be suitable as technical note demonstrating how Mualem model can be modied to reach closed form analytical expressions.}

We intend to keep the article short, so it may continue to qualify as a Methods Note.
\end{itemize}

\section{Reviewer 2}

\textit{\footnotesize The authors developed a closed-form model for hydraulic conductivity in unsaturated zones which is better than available models in the sense that it contains physically meanful parameters. I suggest that it is published in GW as a technical note after the following comments are fully addressed.}

\begin{enumerate}
\item \textit{\footnotesize Add some background in order to reach broader audience;}

A motivational paragraph has been added to the beginning of the introduction.

\item \textit{\footnotesize Add more discussion in term of the physcial meaning of the model developed and the fitted parameters in the examples;}

The model is an extension and application of the lognormal model of Kosugi (1994; 1996). Kosugi develops these models more fully in his original manuscripts.  Mean and variance of the log pore-size distribution are physically based parameters shared with Kosugi's model, while the minimum and maximum pore sizes are also clearly physically based.  The model does not have ``exponents'' or ``powers'' which do not have a clear physical meaning. 

The discussion section has been modified to make this point clearer (lines 232--234 of revised manuscript).

\item \textit{\footnotesize Discussion on Line 144--149 is confusing. Please clarify;}

The discussion regarding the fitting of the 3 and 4-parameter models the Beit Netofa clay has been re-worded for clarity (lines 181--186 in revised manuscript). 

\item \textit{\footnotesize In the table, how come $r_{max}$ of the three-parameter model for the silt loam G.E.3 is 3.11 which two orders of magnitude larger than others?}

We have adjusted this parameter, which was chosen to be somewhat non-physically large in the unconstrained optimization process.  The model-data fit with the smaller $r_{max}$ is not visibly different.  

\item \textit{\footnotesize Enlarge Fig. 1 and 2 as the lables are difficult to read;}

Fonts in figures have been enlarged and all figures are provided as vector drawings.

\item \textit{\footnotesize On Line 52, delete "The" in the section title to be consistent with other titles;}

Deleted

\item \textit{\footnotesize What are the red and yellow circles in Fig. 3,4 and 5.}

Red circles are observed moisture retention curve data. Red circles are observed relative hydraulic conductivity data. See figure captions for references to data sources.

\end{enumerate}


\section{Reviewer 3 (AE)}

\subsection{REMARKS TO AUTHORS:}

\textit{\footnotesize The authors developed an approximate closed-form three-parameter model for unsaturated hydraulic conductivity. Kosugi (1996) developed the numerical expression for the three-parameter model but did not provide a three-parameter closed form because of the difficulty in analytically integrating the expression of Mualem (1976). Kosugi (1996), however, made an assumption that the maximum pore radius, $r_{max}$, approaches infinity and provided a closed form for the two-parameter model.}

\textit{\footnotesize The authors also extended the three-parameter lognormal pore size distribution to a four-parameter model, truncating the pore size distribution at both minimum and maximum pore radii. They developed the corresponding four-parameter model for moisture retention and the associated closed-form expression for unsaturated hydraulic conductivity.}

\begin{itemize}
\item \textit{\footnotesize Some more general context as to the application of this work and how it can be used to better estimate unsaturated hydraulic conductivity would be helpful to readers. The proposed application and associated limitations are not clear. How would these equations be used and why are they an improvement on currently available estimation techniques?}

We have added additional introductory and motivating discussion to the introduction. This includes motivation for developing closed-form expressions for hydraulic conductivity over numerically integrated ones.  Our solution extends the lognormal model (based upon lognormal pore-size distributions) to closed-form hydraulic conductivity models for models with more than 2 parameters.

\item \textit{\footnotesize The abstract could be improved by providing the reader with a better the essence of the research.}

An additional motivating sentence has been added to the abstract.
\end{itemize}

\textit{\footnotesize In my opinion, the paper does not provide a significant scientific contribution since the parameters seem to have deviated from their physical meanings and in essence are more like fitting parameters (see my detailed comments below). The authors also provided the approximate closed form equations without attempting to analyze the order of error associated with using these formulae.}

\textit{\footnotesize In some cases the derivation of the equations was difficult to follow and the units appear to be inconsistent which leads to me to suspect that there is an error somewhere in their derivation. The authors should conduct a unit analysis of the equations to be sure that an error has not been made.}

\subsection{GENERAL COMMENTS}

\begin{itemize}
\item \textit{\footnotesize Rigorous analysis is needed to assess the level of error expected by using the introduced closed form expressions and also to highlight any associated limitations. For example, it seems that these expressions do not provide accurate results if water content is close to residual or saturated moisture content. The authors should quantitatively determine how close to these limits the error becomes unacceptably large and provide recommendations as to the appropriate use of the equations.}

Figures 3--5 exemplify the level of approximation involved in the assumption used to derive equation 7.  We have added more discussion of the nature of the approximation, immediately after its introduction (lines 125--128 of revised manuscript). 

We have added a sentence to the discussion section (lines 217--220 of revised manuscript) which indicate users should compare the approximate closed form solution to the numerically integrated one to rigorously determine if it will work for their case.

\item \textit{\footnotesize Most figures and tables need better explanation and also the quality of the figures is poor and almost illegible. See below for more comments on specific figures.}

Fonts have been enlarged and all figures are provided as vector drawings.

\item \textit{\footnotesize I believe the $\log$ referred to in the paper is the natural logarithm and not base 10 logarithm. The paper should clarify this.}

As you inferred, we follow the mathematical convention of $\log$ as the natural logarithm and any other logarithm base (10, 2, etc.) is specified explicitly. To be completely explicit, we have changed $\log \rightarrow \ln$, which is common in engineering and physics.

\item \textit{\footnotesize Equations in the paper may be better rewritten in terms of parameters $\mu_z$ and $\sigma_z$ without introducing the new parameter $\mu_\eta$.}

We will leave the frequently used parameter $\mu_\eta$ in the derivation, as this simplifies the resulting expressions.  

\item \textit{\footnotesize Also many of these equations do not seem to be correct as they are not consistent in units; this needs to be checked.}

We have corrected a typo in equation 2, which led to inconsistent units. We have double-checked all the equations in the manuscript again for correctness and dimensional consistency.

\end{itemize}

\subsection{DETAILED COMMENTS}
\begin{enumerate}
\item \textit{\footnotesize Line 65: equation 2 appears incorrect. Please check units. $h/\kappa$ has a dimension of L$^{-1}$ and $r/r_{max}$ is dimensionless.}

We have corrected the typo in equation 2 which resulted in the dimensional inconsistency:  $r/r_{max} \rightarrow 1/r_{max}$

\item \textit{\footnotesize Line 68: As discussed in my general comments above, I suggest rewriting Equation 3 to be}

$$f_H (h)=1/(\sqrt{2 \pi} \sigma_z (h-h_c ) ) \exp[-(\log_{10}(h-h_c )-\log_{10}(\kappa + \mu_z ) )/(\sqrt{2} \sigma_z ))〗^2 ]$$

\textit{\footnotesize or even better as}

$$f_H (h)=1/(\sqrt{2 \pi} \sigma_z (h-h_c ) ) \exp[〖-(\log_{10}〖((h-h_c)/\kappa)+\mu_z 〗/(\sqrt{2} \sigma_z ))〗^2 ]$$

\textit{\footnotesize Rewriting the equation as described above, indicates that there may be an error as the expression $(h-h_c)/\kappa$ is not dimensionless.}

\begin{itemize}
\item We are not dropping the variable $\mu_\eta$.
\item There is no error in equation 3.
\item The argument of the logarithm does not need to be dimensionless to be correct. 

\textit{Example:}  the argument of $\log(h/h_0)$ would be dimensionless, because it is a ratio of two heads.  This is equivalent to $\log(h)-\log(h_0)$. The arguments of each of the two logarithms are not dimensionless, but their dimensions are consistent.  

\end{itemize}

\item \textit{\footnotesize This applies to equations 4, 8, 10, 11, and 12. It seems as $\mu_\eta$ being defined as $log(\kappa)-\mu_z$ in Line 69 is not correct.}

There is no error in the expression $\mu_\eta = \log(\kappa) - \mu_Z$. (see above argument regarding dimensions of logarithms).

\item \textit{\footnotesize Line 94: based on the assumption $f_H(h) \approx f_H(h)/(h-h_c)$
This condition needs to be revised or explained. Does this mean that the assumption is $(h-h_c) = 1$ or that $h = h_c$. This approximation as it is stated means the approximation is valid only at large pore radii (i.e., water content is close to saturation).}

We have fixed a typo in the statement of the assumption on line 94 (line 121 of the revised manuscript): $f_H(h) \approx f_H(h)/(h-h_c) \rightarrow f_H(h)/h \approx f_H(h)/(h-h_c)$

\item \textit{\footnotesize Table 1: Although, it can be inferred, the authors need to have another column in the table for type of analysis in each row (i.e., three-parameter or four-parameter).}

We do not feel this needs to be changed.  The rows with three parameters correspond to the three-parameter model, and those with four parameters correspond to the four-parameter model. Additionally indicating such will only make the table more complex. We did add horizontal rules to the table to separate the soils more clearly.

\item \textit{\footnotesize Also, in this table it shows that $r_{max}$ values are distinctively different in each analysis type. Does not that mean the parameter deviated from its physical basis and became just a fitting parameter? If so, the authors should not claim that parameters have physical basis. Please, elaborate.}

Every mathematical model is an abstraction and simplification of reality.  We feel the four-parameter model is more physically plausible than models which do not allow this flexibility (i.e., they assume the maximum pore size is infinite) --- all soils actually have a finite minimum and maximum pore size.

When there is a structural shortcoming in the mathematical model, the remaining model parameters must compensate. We feel when there is a structural shortcoming in a model where pore size is assumed to include both infinitely large and small pores to allow analytical derivation of a closed-form solution.  Not all soils need four-parameter moisture retention or hydraulic conductivity models.  In some soils the two- or three-parameter models are good enough.  Some soils may find the fits from the two- and three-parameter models to be lacking, and all the parameter values fit to such soils would likely be suspect.  

We have added a couple sentences to the discussion section making this point clearer (lines 225--232 of the revised manuscript).

\item \textit{\footnotesize Figures 1, 3, 4, and 5: In the caption, it says a) and b) but they are not shown in the figure.}

Updated figures with (a) and (b) labels are provided as vector drawings.

\item \textit{\footnotesize Figure 2: The 4-parameters deviates from classical capillary pressure head at both lower and upper limbs of the function. Does it mean that the function is valid only if saturation is in the middle between residual and saturated water content? A quantitative tool is needed to estimate the order of expected error in advance.}

The minimum and maximum pore size parameters are not approximations, they are physically based parameters.  Real-world soils do not have infinitely large or small pore sizes, but the non-truncated pore-size probability distribution function does include these very large and very small pores (with very low probability).  

\underline{The 4-parameter and 3-parameter PDFs are not approximations to the 2-parameter PDF}.  We would contend the 2-parameter PDF (which includes infinitely large and small pores) is an approximation of the more physically plausible 4-parameter PDF.  Previously, the 4-parameter distribution did not have a corresponding closed-form hydraulic conductivity distribution.  The modification to the theory of Mualem presented here allows the closed-form expression to be extended to the more physically realistic 4-parameter model.  

We have added two sentences after the discussion of Figure 2 to clarify this (lines 146--148 of the revised manuscript).

\item \textit{\footnotesize All Figures: low quality figures}

Figures are provided as vector drawings.

\item \textit{\footnotesize Lines 175 to 185: I don't agree with the authors' discussion here. Having better fit data is expected as they introduce more fitting parameters. However, these parameters appear to have resulted in much different $r_{max}$ values, which may indicate a departure from physical significance.}

We agree the physical significance is lacking in the three-parameter model, when the three and four parameter models deviate from one another.

All soils have finite minimum and maximum pore sizes. In some soils the approximations that $r_{max} \rightarrow \infty$ or $r_0 \rightarrow 0$ may be adequate.  In other soils these approximations may not be appropriate.

\end{enumerate}

\subsection{ORGANIZATION AND STYLE / EVALUATION}

\begin{itemize}
\item \textit{\footnotesize Well written and organized, however more explanation is needed in some parts.}

An additional motivation paragraph was added at the beginning of the introduction.

\item \textit{\footnotesize Usage of English needs edits.}

We have edited to the manuscript to keep language correct and succinct.

\item \textit{\footnotesize Major revision is required}

We have fixed the mistakes pointed out by the reviewers, and added significant introductory material.  

\end{itemize}

\section{Reviewer 4}

\textit{\footnotesize Overall, I find the paper to be very clearly written and the deviations of the models are also rigorous. And I only have the following minor comments for the authors to consider:}

\begin{itemize}
\item \textit{\footnotesize There are already many other models available for unsaturated hydraulic conductivity and moisture content. Therefore, I believe the paper can be strengthened by including a direct comparison or an explicit discussion on the advantages and disadvantages of the proposed models with a few other commonly adopted ones. The comparison or discussion should at the minimum explain why readers should consider adopting the proposed models.}

A significant numerical comparison between many different models would be beyond the scope of this Methods Note.  We intentionally used the same soils used by van Genuchten (1980) and Kosugi (1994), to provide an additional level of comparison if the reader chooses to compare those papers (we have amplified this similarity on line 162 of the revised manuscript).

\item \textit{\footnotesize Can the authors elaborate more on the assumption made in line 94?}

There was a typo in the original formulation of the assumption (line 121 of the revised manuscript).  The typo has been fixed, and the nature of the approximation has been elaborated upon.

\item \textit{\footnotesize The organization of the paper is different from the "standard" format. Some methods are included in "Introduction", while some introductory information is listed under "Theory". There is also no explicit "Results" section. In fact, I do not have problem following the paper. I am just not sure whether it is an issue to the journal or other readers.}

We have added more introductory material to the beginning, and feel the current sections represent their content.  We hope this makes the paper more conformant with the reviewers' and editors' opinions about journal and article style.

\end{itemize}

\end{document}
