# $Id: plot_data.py,v 1.6 2008/06/17 19:50:15 kris Exp kris $

import pickle
import mrc_models
import kh_models
from numpy import array
import os
import math

data = pickle.load(open('mrc_kh_pickle','r'))
vgresults = pickle.load(open('vgt_fit_results_pickle','r'))
lnresults = pickle.load(open('fpln_fit_results_pickle','r'))
mvgresults = pickle.load(open('mvgt_fit_results_pickle','r'))
lnkresults = pickle.load(open('kfpln_fit_results_pickle','r'))

# labels for USDA texture classes
USDA = ['sand','loam','silt','clay']
USDAsub = ['sand','loamy sand','sandy loam','sandy clay loam',
           'sandy clay','clay','clay loam','loam','silty loam',
           'silt','silty clay loam','silty clay']

for id in data:
    tmpmrcfile = open('tmp.gnuplot','w')
    for hv,tv in zip(data[id]['mrc']['h'],data[id]['mrc']['t']):
        tmpmrcfile.write(str(tv) + ' ' + str(hv) + '\n')
    tmpmrcfile.close()

    tmpsscfile = open('ssc.tmp','w')
    tmpsscfile.write(str(data[id]['ssc'][0]) + ' ' +
                     str(data[id]['ssc'][1]) + ' ' +
                     str(data[id]['ssc'][2]) + '\n')
    tmpsscfile.close()

    tex = USDA[data[id]['USDA']]
    texsub = USDAsub[data[id]['USDAsub']]
    
    # evenly log-spaced vector of values to compute vg curve for
    hmin = math.log10(min(data[id]['mrc']['h']))-0.25
    hmax = math.log10(max(data[id]['mrc']['h']))+0.25
    numh = 100
    hcalc = [math.pow(10.0, hmin + (hmax - hmin)*i/float(numh-1)) for i in range(numh)]

    # compute van Genuchten model using optimized parameters
    tvg = mrc_models.vgt(vgresults[id]['x'],array(hcalc))

    # results of optimizing just thr,ths,mu and sigma, using r0 and rmax from K(h) fitting
    x1 = lnresults[id]['x1'].tolist()
    x1[0] = x1[0]*x1[1]
    x1.insert(2,math.pow(10.0, lnkresults[id]['x'][0] + lnkresults[id]['x'][1]))
    x1.insert(3,math.pow(10.0, lnkresults[id]['x'][1]))

    # compute 4-parameter lognormal model using optimized parameters
    fpln1 = mrc_models.fourpln(x1,array(hcalc))

    ob1 =  vgresults[id]['obj']
    thr1,ths1,alf,n = vgresults[id]['x'][0:4]
    ob2 =  lnresults[id]['f1']
    thrmult,ths2,r0,rmax,sig,mu = x1
    thr2 = ths2*thrmult

    tmpvgfile = open('tmp.vg','w')
    for hv,vg in zip(hcalc,tvg):
        tmpvgfile.write(str(vg) + ' '+ str(hv) + '\n')
    tmpvgfile.close()

    tmpvgfile = open('tmp1.ln','w')
    for hv,ln in zip(hcalc,fpln1):
        tmpvgfile.write(str(ln) + ' '+ str(hv) + '\n')
    tmpvgfile.close()

    gnuplotmrc = os.popen('gnuplot','w')
    gnuplotmrc.write("""set logscale y
set xlabel '{/Symbol q}'
set ylabel '-h'
unset key
set format y '10^{%%L}'
set title '%(texsub)s %(id)s moisture retention curve'
set terminal postscript eps enhanced color linewidth 3.0
set output '%(tex)s_mrc_%(id)s.eps'
set multiplot
set origin 0.0,0.0
set size 1.0,1.0
set key bottom right
set label  "{/Symbol q}_r = %(thr1)5.3f" at graph 0.025, graph 0.25
set label  "{/Symbol q}_s = %(ths1)5.3f" at graph 0.025, graph 0.2
set label  "{/Symbol a} = %(alf)5.3g"    at graph 0.025, graph 0.15
set label  "n = %(n)5.3g"                at graph 0.025, graph 0.1
set label  "vG O_w(p) = %(ob1)5.2g"      at graph 0.025, graph 0.05
set label  "{/Symbol q}_r = %(thr2)5.3f" at graph 0.25, graph 0.35
set label  "{/Symbol q}_s = %(ths2)5.3f" at graph 0.25, graph 0.3
set label  "r_0 = %(r0)5.3g"             at graph 0.25, graph 0.25
set label  "r_{max} = %(rmax)5.3g"       at graph 0.25, graph 0.2
set label  "{/Symbol m}_Z = %(mu)5.3g"   at graph 0.25, graph 0.15
set label  "{/Symbol s}_Z = %(sig)5.3g"  at graph 0.25, graph 0.1
set label  "ln O_w(p) = %(ob2)5.2g"         at graph 0.25, graph 0.05
plot 'tmp.gnuplot' using 1:2 notitle 'data' with points, \
     'tmp.vg' using 1:2 title 'vG' with lines, \
     'tmp1.ln' using 1:2 title 'LN 2-step' with lines
unset logscale x
unset logscale y
unset xlabel
unset ylabel
unset label
unset title
set origin 0.6,0.55
set size 0.4,0.4
load 'ternary.gnu'
plot 'ssc.tmp' using (gx($1,$3)):(gz($1,$3)) notitle with points 7 
set nomultiplot""" % vars())
    gnuplotmrc.close()
    
    tmpkhfile = open('tmp.gnuplot','w')
    for hv,kv in zip(data[id]['kh']['h'],data[id]['kh']['k']):
        tmpkhfile.write(str(hv) + ' ' + str(kv) + '\n')
    tmpkhfile.close()

    # evenly log-spaced vector of values to compute mvg curve
    hmin = min(data[id]['kh']['h'])
    if hmin < 1.0:
        hmin == -0.25
    else:
        hmin = math.log10(hmin)-0.25

    hmax = math.log10(max(data[id]['kh']['h']))+0.25
    hcalc = [math.pow(10.0, hmin + (hmax - hmin)*i/float(numh-1)) for i in range(numh)]
    
    # van Genuchten results
    p = vgresults[id]['x'].tolist()
    p.extend(mvgresults[id]['x'].tolist())
    p[4] = math.pow(10.0,p[4])

    kvg = kh_models.mvgt(p,array(hcalc))

    # 4-parameter lognormal
    p = lnresults[id]['x1'].tolist()
    p.append(math.pow(10.0,lnkresults[id]['x'][2])) # K0
    p.append(lnkresults[id]['x'][3])  # L
    p.insert(2,math.pow(10.0,lnkresults[id]['x'][0]+lnkresults[id]['x'][1])) # r0 
    p.insert(3,math.pow(10.0,lnkresults[id]['x'][1])) # rmax
    kln = kh_models.fourplnk(p,array(hcalc))

    tmpvgfile = open('tmp.vg','w')
    for hv,vg in zip(hcalc,kvg):
        tmpvgfile.write(str(hv) + ' ' + str(vg) + '\n')
    tmpvgfile.close()

    tmplnfile = open('tmp.ln','w')
    for hv,ln in zip(hcalc,kln):
        tmplnfile.write(str(hv) + ' ' + str(ln) + '\n')
    tmplnfile.close()

     # van Genuchten results
    ob1 = mvgresults[id]['obj']
    ks1 = math.pow(10.0,mvgresults[id]['x'][0])
    l1 = mvgresults[id]['x'][1]

    # 4-parameter ln
    ob2 = lnkresults[id]['obj']
    ks2 = math.pow(10.0,lnkresults[id]['x'][2])
    l2 = lnkresults[id]['x'][3]
    r0,rmax = [math.pow(10.0,lnkresults[id]['x'][0]+lnkresults[id]['x'][1]),
               math.pow(10.0,lnkresults[id]['x'][1])]

    gnuplotkh = os.popen('gnuplot','w')
    gnuplotkh.write("""set logscale y
set logscale x
set xlabel "-h"
set ylabel "K"
set format y "10^{%%L}"
set format x "10^{%%L}"
set terminal postscript eps enhanced color linewidth 3.0
set output '%(tex)s_kh_%(id)s.eps'
set title '%(texsub)s hydraulic conductivity curve %(id)s'
set key bottom right
set multiplot
set origin 0.0,0.0
set size 1.0,1.0
set label  "vG O_K(p) = %(ob1)5.3g"  at graph 0.025, graph 0.15
set label  "vG K_s = %(ks1)5.3g"     at graph 0.025, graph 0.1
set label  "vG L = %(l1)5.3g"        at graph 0.025, graph 0.05
set label  "r_0 = %(r0)5.3g"         at graph 0.25, graph 0.25
set label  "r_{max} = %(rmax)5.3g"   at graph 0.25, graph 0.20
set label  "ln O_K(p) = %(ob2)5.3g"  at graph 0.25, graph 0.15
set label  "ln K_s = %(ks2)5.3g"     at graph 0.25, graph 0.1
set label  "ln L = %(l2)5.3g"        at graph 0.25, graph 0.05
plot 'tmp.gnuplot' using 1:2 notitle 'kdat' with points, \
     'tmp.vg' using 1:2 title 'mvG' with lines, \
     'tmp.ln' using 1:2 title '4pLN 2-step' with lines
unset key
unset logscale x
unset logscale y
unset label
unset xlabel
unset ylabel
unset title
set origin 0.6,0.55
set size 0.4,0.4
load 'ternary.gnu'
plot 'ssc.tmp' using (gx($1,$3)):(gz($1,$3)) notitle with points 7 
set nomultiplot""" % vars())
    gnuplotkh.close()


