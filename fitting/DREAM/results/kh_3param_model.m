function k = kh_3param_model(p,h)

  kap  = 0.149;
  %thr  = p(1);
  %ths  = p(2);
  rmax = p(3);
  sigz = p(4);
  muz  = p(5);
  K0   = p(6);

  Se = mrc_th3param_model(p(1:5),h);

  hc   = kap/rmax;
  memsz2 = (-1.9038 - muz) - sigz^2;

  mink = 1.0E-100;
  rt2z = 1.4142135623730951*sigz;

  k = zeros(1,length(h));
    
  for i=1:length(h)
    if h(i) <= hc
      k(i) = K0;
    else
      k(i) = max( mink, K0*sqrt(Se(i))* ...
                 (0.5*erfc((log(h(i) - hc) - memsz2)/rt2z))^2 );
    end
  end

        
