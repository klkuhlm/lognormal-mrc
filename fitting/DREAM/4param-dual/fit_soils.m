function [modtheta] = fit_soils(par,extra)

  p = par(1:6);
  p(3) = 10.0^par(3);
  p(4) = 10.0^par(4);
  
  modtheta = mrc_model(p,extra.ht);
  