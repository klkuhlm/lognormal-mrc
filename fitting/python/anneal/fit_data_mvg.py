# $Id: fit_data_mvg.py,v 1.2 2008/06/17 19:43:14 kris Exp kris $

import pickle
import kh_models
import math
from numpy import array,log10
from scipy.optimize import fmin_l_bfgs_b,anneal

data = pickle.load(open('mrc_kh_pickle','r'))
vgresults = pickle.load(open('vgt_fit_results_pickle','r'))


# wrapper for calling mvg routine
def mvg_ls(pin, *args):
    h = array(args[0]['h']) 
    k = array(args[0]['k']) 

    pout = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    pout[0] = args[1][0] # thr  <- from mrc optimization
    pout[1] = args[1][1] # ths
    pout[2] = args[1][2] # alpha
    pout[3] = args[1][3] # n
    pout[4] = 10**(pin[0]) # ksat
    pout[5] = pin[1]  # L

    return sum((log10(kh_models.mvgt(pout,h)) - log10(k))**2)

# results
mvgresults = {}

fres = open('mvgt.out','w')

# loop over soils 
for id in vgresults:
    minp = [math.log10(1.0E-3),-10.0]
    maxp = [math.log10(1.0E+7), 10.0]

    xa,da = anneal(mvg_ls, [log10(data[id]['ks2']),0.5], 
                   lower=minp,upper=maxp,args=[data[id]['kh'], 
                                               vgresults[id]['x']])
    args=[data[id]['kh'],vgresults[id]['x']]
    fa = mvg_ls(xa,*args)

    x,f,d = fmin_l_bfgs_b(mvg_ls, xa, approx_grad=1, 
             bounds=zip(minp,maxp),args=[data[id]['kh'], 
                                         vgresults[id]['x']])
    
    print id, fa,f,x

    mvgresults[id] = {'x':x,'obj':f,'info':d}

    fres.write(id + ' ' + str(x) + ' ' + str(f) + '\n')
    

fres.close()
pickle.dump(mvgresults, open('mvgt_fit_results_pickle','w'))
