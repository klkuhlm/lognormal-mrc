
% load t.mat

% log-log plots
for i=1:9
  figure(i)
  loglog(t,-S((i-1)*50+1:i*50,1:10),':');  % random locations
%   v = axis;
  axis([1.0E-4, 1.0,1.0E-4, 1]);
  hold on;
  plot(t,-S((i-1)*50+1:i*50,11),'-','LineWidth',2);  % true results
  xlabel('time')
  ylabel('drawdown')
  title(['10 random locations vs true results at observation point ' num2str(i)])
end

for i=1:9
  figure(9+i)
  semilogx(t,-S((i-1)*50+1:i*50,1:10),':');  % random locations
%   v = axis;
  axis([1.0E-4, 1.0,1.0E-4, 1]);
  hold on;
  plot(t,-S((i-1)*50+1:i*50,11),'-','LineWidth',2);  % true results
  xlabel('time')
  ylabel('drawdown')
  title(['10 random locations vs true results at observation point ' num2str(i)])
end