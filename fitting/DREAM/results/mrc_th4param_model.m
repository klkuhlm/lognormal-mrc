function t = mrc_th4param_model(p,h)

% return dimensionless theta*

kap  = 0.149;
%thr  = p(1);
%ths  = p(2);
r0   = p(3);
rmax = p(4);
sigz = p(5);
muz  = p(6);

hmax = kap/r0;
hc   = kap/rmax;

mueta = -1.9038 - muz;  % log(0.0149) = -1.9038
rt2z  = 1.4142135623730951*sigz;

t = zeros(1,length(h));

for i=1:length(h)
  if h(i) >= hmax
    t(i) = 0.0;
  elseif h(i) <= hc
    t(i) = 1.0;
  else
    t(i) = 0.5*erfc((log(1.0/(1.0/h(i) - 1.0/hmax) - hc)-mueta)/rt2z);
  end
end
